import 'package:curved_navigation_bar/curved_navigation_bar.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:qimma_client/pages/account/account_page.dart';
import 'package:qimma_client/pages/cart/cart_page.dart';
import 'package:qimma_client/pages/chat/people_chat_page.dart';
import 'package:qimma_client/pages/home/home_content.dart';
import 'package:qimma_client/pages/notifications/notifications_page.dart';
import 'package:qimma_client/providers/page_provider.dart';
import 'package:qimma_client/utils/consts.dart';

class MyBottomNavigatonBar extends StatefulWidget {
  @override
  _MyBottomNavigatonBarState createState() => _MyBottomNavigatonBarState();
}

class _MyBottomNavigatonBarState extends State<MyBottomNavigatonBar> {
  @override
  Widget build(BuildContext context) {
    return Consumer<PageProvider>(
      builder: (context, provider, child) => CurvedNavigationBar(
        color: mainColor,
        backgroundColor: Colors.transparent,
        height: 60,
        // best height
        index: provider.pageIndex,
        // initial index
        buttonBackgroundColor: mainColor,
        animationDuration: Duration(milliseconds: 300),
        onTap: (index) {
          if (index == 0) {
            provider.setPage(index, HomeContent());
          } else if (index == -1) {
            //provider.setPage(index, PeopleChatPage());
          } else if (index == 1) {
            provider.setPage(index, CartPage());
          }  else if (index == 2) {
            provider.setPage(index, NotificationsPage());
          } else if (index == 3) {
            provider.setPage(index, AccountPage());
          }
        },
        items: <Widget>[
          Icon(Icons.home, color: Colors.white,),
          //Icon(FontAwesomeIcons.envelope, color: Colors.white, size: 20,),
          Icon(Icons.add_shopping_cart, color: Colors.white),
          Icon(Icons.notifications, color: Colors.white),
          Icon(Icons.person, color: Colors.white),
        ],
      ),
    );
  }

  void goToPage(Widget page) {
    Navigator.of(context).push(MaterialPageRoute(builder: (_) => page));
  }
}
