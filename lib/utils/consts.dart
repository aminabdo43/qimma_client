
import 'package:flutter/material.dart';

final String appName = 'Qimma';

final Color mainColor = Color(0xff01AC10);
final Color secondColor = Color(0xffD4E1EC);
final Color googleColor = Color(0xffCC4C41);
final Color redColor = Color(0xffff2f1f);
final Color facebookColor = Color(0xff4267B2);
final Color scaffoldBackgroundColor = Color(0xffF7F7F7);