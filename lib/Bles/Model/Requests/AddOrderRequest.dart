import 'dart:io';

import 'package:dio/dio.dart';
import 'package:qimma_client/utils/base/BaseRequest.dart';

class AddOrderRequest extends BaseRequest {
  String paymentMethod;
  String addressId;
  File bank_trans_receipt;
  String bank_name;
  String notes;


  AddOrderRequest(
      {this.paymentMethod,
      this.addressId,
      this.bank_trans_receipt,
      this.bank_name,
      this.notes});

  static AddOrderRequest fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AddOrderRequest addOrderRequestBean = AddOrderRequest();
    addOrderRequestBean.paymentMethod = map['payment_method'];
    addOrderRequestBean.addressId = map['address_id'];
    return addOrderRequestBean;
  }


  @override
  String toString() {
    return 'AddOrderRequest{paymentMethod: $paymentMethod, addressId: $addressId, bank_trans_receipt: $bank_trans_receipt, bank_name: $bank_name, notes: $notes}';
  }

  Future<Map<String, dynamic>> toJson() async {
    if(bank_trans_receipt == null ){
      return {
        "payment_method": paymentMethod,
        "address_id": addressId,
        "bank_name": bank_name,
        "notes": notes,
      };
    }
    try {
      return {
        "payment_method": paymentMethod,
        "address_id": addressId,
        "bank_trans_receipt": await (await MultipartFile.fromFile(
            bank_trans_receipt?.path ?? null)) ?? null,
        "bank_name": bank_name,
        "notes": notes,
      };
    }catch(e){
      return {
        "payment_method": paymentMethod,
        "address_id": addressId,
        "bank_name": bank_name,
        "notes": notes,
      };
    }
  }
}