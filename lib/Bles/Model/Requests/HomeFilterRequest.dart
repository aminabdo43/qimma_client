import 'package:qimma_client/utils/base/BaseRequest.dart';

// 1 = Price DESC , 2 = Price ASC , 3 = rate DESC
class HomeFilterRequest extends BaseRequest {
  List<int> catId;
  int priceFrom;
  int priceTo;
  String name;
  int type;


  HomeFilterRequest(
      {this.catId, this.priceFrom = 0 , this.priceTo = 999999, this.name = "", this.type = 2});

  static HomeFilterRequest fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    HomeFilterRequest homeFilterRequestBean = HomeFilterRequest();
    homeFilterRequestBean.catId = List()..addAll(
      (map['cat_id'] as List ?? []).map((o) => int.tryParse(o.toString()))
    );
    homeFilterRequestBean.priceFrom = map['price_from'];
    homeFilterRequestBean.priceTo = map['price_to'];
    homeFilterRequestBean.name = map['name'];
    homeFilterRequestBean.type = map['type'];
    return homeFilterRequestBean;
  }

  Map toJson() => {
    "cat_id": catId,
    "price_from": priceFrom ?? '0',
    "price_to": priceTo ?? '9999999',
    "name": name ?? '',
    "type": type,
  };
}