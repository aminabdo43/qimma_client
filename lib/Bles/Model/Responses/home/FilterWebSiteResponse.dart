import 'package:flutter/cupertino.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/base/BaseResponse.dart';

class FilterWebSiteResponse extends BaseResponse{
  DataBean data;
  int status;
  String message;

  static FilterWebSiteResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    FilterWebSiteResponse filterWebSiteResponseBean = FilterWebSiteResponse();
    filterWebSiteResponseBean.data = DataBean.fromMap(map['data']);
    filterWebSiteResponseBean.status = map['status'];
    filterWebSiteResponseBean.message = map['message'];
    return filterWebSiteResponseBean;
  }

  Map toJson() => {
    "data": data,
    "status": status,
    "message": message,
  };
}

class DataBean {
  List<ProductsBean> Offers;
  List<ProductsBean> products;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.Offers = List()..addAll(
      (map['Offers'] as List ?? []).map((o) => ProductsBean.fromMap(o))
    );
    dataBean.products = List()..addAll(
      (map['products'] as List ?? []).map((o) => ProductsBean.fromMap(o))
    );
    return dataBean;
  }

  Map toJson() => {
    "Offers": Offers,
    "products": products,
  };
}

class ProductsBean {
  dynamic id;
  dynamic mainProductId;
  dynamic mainProductName;
  dynamic desc;
  dynamic Difference;
  dynamic image;
  dynamic rate;
  dynamic isOffer;
  dynamic offerAmount;
  dynamic lat;
  dynamic lng;
  dynamic distance;
  dynamic isFavorite;
  dynamic InventoryId;
  dynamic InventoryName;
  dynamic Quantity;
  List<ColorsBean> colors;
  List<SizesBean> sizes;
  List<ImagesBean> images;
  String PurchasingPrice;
  String WholesaleWholesalePrice;
  String WholesalePrice;
  dynamic SellingPrice;
  String barcode;

  static ProductsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProductsBean productsBean = ProductsBean();
    productsBean.id = map['id'];
    productsBean.mainProductId = map['main_product_id'];
    productsBean.mainProductName = map['main_Product_name'];
    productsBean.desc = map['desc'];
    productsBean.Difference = map['Difference'];
    productsBean.image = map['image'];
    productsBean.rate = map['rate'];
    productsBean.isOffer = map['is_offer'];
    productsBean.offerAmount = map['offer_amount'];
    productsBean.lat = map['lat'];
    productsBean.lng = map['lng'];
    productsBean.distance = map['distance'];
    productsBean.isFavorite = map['is_favorite'];
    productsBean.InventoryId = map['Inventory_id'];
    productsBean.InventoryName =  "";//map['Inventory_name'];
    productsBean.Quantity = map['Quantity'];
    productsBean.colors = List()..addAll(
      (map['colors'] as List ?? []).map((o) => ColorsBean.fromMap(o))
    );
    productsBean.sizes = List()..addAll(
      (map['sizes'] as List ?? []).map((o) => SizesBean.fromMap(o))
    );
    productsBean.images = List()..addAll(
      (map['images'] as List ?? []).map((o) => ImagesBean.fromMap(o))
    );
    productsBean.PurchasingPrice = map['Purchasing_price'];
    productsBean.WholesaleWholesalePrice = map['Wholesale_wholesale_price'];
    productsBean.WholesalePrice = map['Wholesale_price'];
    productsBean.SellingPrice = map['price'];
    productsBean.barcode = map['barcode'];


    if(AppUtils.userData.status == 1){
      productsBean.SellingPrice = productsBean.SellingPrice;
    }
    else if(AppUtils.userData.status == 3){
      productsBean.SellingPrice = productsBean.WholesalePrice;
    }
    else if(AppUtils.userData.status == 4){
      productsBean.SellingPrice = productsBean.WholesaleWholesalePrice;
    }
    return productsBean;
  }

  Map toJson() => {
    "id": id,
    "main_product_id": mainProductId,
    "main_Product_name": mainProductName,
    "desc": desc,
    "Difference": Difference,
    "image": image,
    "rate": rate,
    "is_offer": isOffer,
    "offer_amount": offerAmount,
    "lat": lat,
    "lng": lng,
    "distance": distance,
    "is_favorite": isFavorite,
    "Inventory_id": InventoryId,
    "Inventory_name": InventoryName,
    "Quantity": Quantity,
    "colors": colors,
    "sizes": sizes,
    "images": images,
    "Purchasing_price": PurchasingPrice,
    "Wholesale_wholesale_price": WholesaleWholesalePrice,
    "Wholesale_price": WholesalePrice,
    "Selling_price": SellingPrice,
    "barcode": barcode,
  };
}

class ImagesBean {
  int id;
  String image;

  static ImagesBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ImagesBean imagesBean = ImagesBean();
    imagesBean.id = map['id'];
    imagesBean.image = map['image'];
    return imagesBean;
  }

  Map toJson() => {
    "id": id,
    "image": image,
  };
}

class SizesBean {
  int id;
  String name;

  static SizesBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SizesBean sizesBean = SizesBean();
    sizesBean.id = map['id'];
    sizesBean.name = map['name'];
    return sizesBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
  };
}

class ColorsBean {
  int id;
  String colorCode;

  static ColorsBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ColorsBean colorsBean = ColorsBean();
    colorsBean.id = map['id'];
    if(map['color_code'].contains("rgb")){
      colorsBean.colorCode = "${Color(hexOfRGBA(0,0,0,opacity: 0.7))}";
    }
    else{
      colorsBean.colorCode = map['color_code'];
    }
    return colorsBean;
  }

  Map toJson() => {
    "id": id,
    "color_code": colorCode,
  };
  static int hexOfRGBA(int r,int g,int b,{double opacity=1})
  {
    r = (r<0)?-r:r;
    g = (g<0)?-g:g;
    b = (b<0)?-b:b;
    opacity = (opacity<0)?-opacity:opacity;
    opacity = (opacity>1)?255:opacity*255;
    r = (r>255)?255:r;
    g = (g>255)?255:g;
    b = (b>255)?255:b;
    int a = opacity.toInt();
    return int.parse('0x${a.toRadixString(16)}${r.toRadixString(16)}${g.toRadixString(16)}${b.toRadixString(16)}');
  }
}

