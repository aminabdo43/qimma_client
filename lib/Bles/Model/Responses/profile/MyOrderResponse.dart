import 'package:qimma_client/utils/base/BaseResponse.dart';

class MyOrderResponse extends BaseResponse{
  List<MyOrdersModel> data;
  String message;
  int status;

  static MyOrderResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MyOrderResponse myOrderResponseBean = MyOrderResponse();
    myOrderResponseBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => MyOrdersModel.fromMap(o))
    );
    myOrderResponseBean.message = map['message'];
    myOrderResponseBean.status = map['status'];
    return myOrderResponseBean;
  }

  Map toJson() => {
    "data": data,
    "message": message,
    "status": status,
  };
}

class MyOrdersModel {
  int id;
  String priceType;
  String prePrice;
  String discountType;
  dynamic discount;
  String tax1Type;
  dynamic tax1;
  String tax2Type;
  dynamic tax2;
  dynamic totalPrice;
  dynamic paid;
  dynamic rest;
  dynamic shippingPrice;
  String status;
  String email;
  String phone;
  String paymentMethod;
  dynamic rate;
  dynamic report;
  String date;
  String name;
  String userId;
  String representativeId;
  String representativeName;
  String lat;
  String lng;
  dynamic addressId;
  String address;
  List<ProductModel> products;
  String bankTransmissionReceipt;
  dynamic bankName;
  dynamic notes;
  String createdAt;
  String updatedAt;

  static MyOrdersModel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MyOrdersModel dataBean = MyOrdersModel();
    dataBean.id = map['id'];
    dataBean.priceType = map['price_type'];
    dataBean.prePrice = map['pre_price'];
    dataBean.discountType = map['discount_type'];
    dataBean.discount = map['discount'];
    dataBean.tax1Type = map['tax1_type'];
    dataBean.tax1 = map['tax1'];
    dataBean.tax2Type = map['tax2_type'];
    dataBean.tax2 = map['tax2'];
    dataBean.totalPrice = map['total_price'];
    dataBean.paid = map['paid'];
    dataBean.rest = map['rest'];
    dataBean.shippingPrice = map['shipping_price'];
    dataBean.status = map['status'];
    dataBean.email = map['email'];
    dataBean.phone = map['phone'];
    dataBean.paymentMethod = map['payment_method'];
    dataBean.rate = map['rate'];
    dataBean.report = map['report'];
    dataBean.date = map['date'];
    dataBean.name = map['name'];
    dataBean.userId = map['user_id'];
    dataBean.representativeId = map['representative_id'];
    dataBean.representativeName = map['representative_name'];
    dataBean.lat = map['lat'];
    dataBean.lng = map['lng'];
    dataBean.addressId = map['address_id'];
    dataBean.address = map['address'];
    dataBean.products = List()..addAll(
      (map['products'] as List ?? []).map((o) => ProductModel.fromMap(o))
    );
    dataBean.bankTransmissionReceipt = map['bank_transmission_receipt'];
    dataBean.bankName = map['bank_name'];
    dataBean.notes = map['notes'];
    dataBean.createdAt = map['created_at'];
    dataBean.updatedAt = map['updated_at'];
    return dataBean;
  }

  Map toJson() => {
    "id": id,
    "price_type": priceType,
    "pre_price": prePrice,
    "discount_type": discountType,
    "discount": discount,
    "tax1_type": tax1Type,
    "tax1": tax1,
    "tax2_type": tax2Type,
    "tax2": tax2,
    "total_price": totalPrice,
    "paid": paid,
    "rest": rest,
    "shipping_price": shippingPrice,
    "status": status,
    "email": email,
    "phone": phone,
    "payment_method": paymentMethod,
    "rate": rate,
    "report": report,
    "date": date,
    "name": name,
    "user_id": userId,
    "representative_id": representativeId,
    "representative_name": representativeName,
    "lat": lat,
    "lng": lng,
    "address_id": addressId,
    "address": address,
    "products": products,
    "bank_transmission_receipt": bankTransmissionReceipt,
    "bank_name": bankName,
    "notes": notes,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}

class ProductModel {
  int id;
  String mainProductId;
  String mainProductName;
  String desc;
  String Difference;
  String image;
  int quantity;
  int price;
  dynamic color;
  dynamic size;

  static ProductModel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProductModel productsBean = ProductModel();
    productsBean.id = map['id'];
    productsBean.mainProductId = map['main_product_id'];
    productsBean.mainProductName = map['main_Product_name'];
    productsBean.desc = map['desc'];
    productsBean.Difference = map['Difference'];
    productsBean.image = map['image'];
    productsBean.quantity = map['quantity'];
    productsBean.price = map['price'];
    productsBean.color = map['color'];
    productsBean.size = map['size'];
    return productsBean;
  }

  Map toJson() => {
    "id": id,
    "main_product_id": mainProductId,
    "main_Product_name": mainProductName,
    "desc": desc,
    "Difference": Difference,
    "image": image,
    "quantity": quantity,
    "price": price,
    "color": color,
    "size": size,
  };
}