import 'package:qimma_client/Bles/Model/Responses/auth/SignupResponse.dart';
import 'package:qimma_client/utils/base/BaseResponse.dart';

class EditProfileResponse extends BaseResponse {
  int status;
  UserData data;
  String message;

  static EditProfileResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    EditProfileResponse editProfileResponseBean = EditProfileResponse();
    editProfileResponseBean.status = map['status'];
    editProfileResponseBean.data = UserData.fromMap(map['data']);
    editProfileResponseBean.message = map['message'];
    return editProfileResponseBean;
  }

  Map toJson() => {
    "status": status,
    "data": data,
    "message": message,
  };
}