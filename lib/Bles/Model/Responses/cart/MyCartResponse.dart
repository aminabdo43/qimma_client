import 'package:qimma_client/utils/base/BaseResponse.dart';

class MyCartResponse extends BaseResponse {
  DataBean data;
  String message;
  int status;

  static MyCartResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MyCartResponse myCartResponseBean = MyCartResponse();
    myCartResponseBean.data = DataBean.fromMap(map['data']);
    myCartResponseBean.message = map['message'];
    myCartResponseBean.status = map['status'];
    return myCartResponseBean;
  }


  Map toJson() => {
    "data": data,
    "message": message,
    "status": status,
  };
}

class DataBean {
  List<ProductsCartBean> products;
  dynamic deliveryCharges;
  dynamic tax;
  dynamic subTotalPrice;
  dynamic totalPrice;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.products = List()..addAll(
      (map['products'] as List ?? []).map((o) => ProductsCartBean.fromMap(o))
    );
    dataBean.deliveryCharges = map['delivery_charges'];
    dataBean.tax = map['tax'];
    dataBean.subTotalPrice = map['sub_total_price'];
    dataBean.totalPrice = map['total_price'];
    return dataBean;
  }

  Map toJson() => {
    "products": products,
    "delivery_charges": deliveryCharges,
    "tax": tax,
    "sub_total_price": subTotalPrice,
    "total_price": totalPrice,
  };
}

class ProductsCartBean {
  int id;
  dynamic name;
  dynamic desc;
  String productDetailDesc;
  dynamic productDetailDifference;
  String image;
  String rate;
  dynamic isOffer;
  dynamic offerAmount;
  dynamic lat;
  dynamic lng;
  bool isFavorite;
  ColorBean color;
  SizeBean size;
  dynamic price;
  dynamic quantity;

  static ProductsCartBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ProductsCartBean productsBean = ProductsCartBean();
    productsBean.id = map['id'];
    productsBean.name = map['name'];
    productsBean.desc = map['desc'];
    productsBean.productDetailDesc = map['product_detail_desc'];
    productsBean.productDetailDifference = map['product_detail_difference'];
    productsBean.image = map['image'];
    productsBean.rate = map['rate'];
    productsBean.isOffer = map['is_offer'];
    productsBean.offerAmount = map['offer_amount'];
    productsBean.lat = map['lat'];
    productsBean.lng = map['lng'];
    productsBean.isFavorite = map['is_favorite'];
    productsBean.color = ColorBean.fromMap(map['color']);
    productsBean.size = SizeBean.fromMap(map['size']);
    productsBean.price = map['price'];
    productsBean.quantity = map['quantity'];
    return productsBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
    "desc": desc,
    "product_detail_desc": productDetailDesc,
    "product_detail_difference": productDetailDifference,
    "image": image,
    "rate": rate,
    "is_offer": isOffer,
    "offer_amount": offerAmount,
    "lat": lat,
    "lng": lng,
    "is_favorite": isFavorite,
    "color": color,
    "size": size,
    "price": price,
    "quantity": quantity,
  };
}

class SizeBean {
  int id;
  String name;

  static SizeBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    SizeBean sizeBean = SizeBean();
    sizeBean.id = map['id'];
    sizeBean.name = map['name'];
    return sizeBean;
  }

  Map toJson() => {
    "id": id,
    "name": name,
  };
}

class ColorBean {
  int id;
  String colorCode;

  static ColorBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ColorBean colorBean = ColorBean();
    colorBean.id = map['id'];
    colorBean.colorCode = map['color_code'];
    return colorBean;
  }

  Map toJson() => {
    "id": id,
    "color_code": colorCode,
  };
}