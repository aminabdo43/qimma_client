import 'package:qimma_client/Bles/Model/Responses/home/FilterWebSiteResponse.dart';

class MyOrdersResponse {
  List<OrderModel> data;
  String message;
  int status;

  static MyOrdersResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    MyOrdersResponse myOrdersResponseBean = MyOrdersResponse();
    myOrdersResponseBean.data = List()..addAll(
      (map['data'] as List ?? []).map((o) => OrderModel.fromMap(o))
    );
    myOrdersResponseBean.message = map['message'];
    myOrdersResponseBean.status = map['status'];
    return myOrdersResponseBean;
  }

  Map toJson() => {
    "data": data,
    "message": message,
    "status": status,
  };
}

class OrderModel {
  int id;
  String priceType;
  String prePrice;
  String discountType;
  int discount;
  String tax1Type;
  int tax1;
  String tax2Type;
  int tax2;
  int totalPrice;
  int paid;
  int rest;
  int shippingPrice;
  String status;
  String email;
  String phone;
  String paymentMethod;
  dynamic rate;
  dynamic report;
  String date;
  String name;
  String userId;
  String representativeId;
  String representativeName;
  String lat;
  String lng;
  int addressId;
  String address;
  List<ProductsBean> products;
  String bankTransmissionReceipt;
  String bankName;
  String notes;
  String createdAt;
  String updatedAt;

  static OrderModel fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    OrderModel dataBean = OrderModel();
    dataBean.id = map['id'];
    dataBean.priceType = map['price_type'];
    dataBean.prePrice = map['pre_price'];
    dataBean.discountType = map['discount_type'];
    dataBean.discount = map['discount'];
    dataBean.tax1Type = map['tax1_type'];
    dataBean.tax1 = map['tax1'];
    dataBean.tax2Type = map['tax2_type'];
    dataBean.tax2 = map['tax2'];
    dataBean.totalPrice = map['total_price'];
    dataBean.paid = map['paid'];
    dataBean.rest = map['rest'];
    dataBean.shippingPrice = map['shipping_price'];
    dataBean.status = map['status'];
    dataBean.email = map['email'];
    dataBean.phone = map['phone'];
    dataBean.paymentMethod = map['payment_method'];
    dataBean.rate = map['rate'];
    dataBean.report = map['report'];
    dataBean.date = map['date'];
    dataBean.name = map['name'];
    dataBean.userId = map['user_id'];
    dataBean.representativeId = map['representative_id'];
    dataBean.representativeName = map['representative_name'];
    dataBean.lat = map['lat'];
    dataBean.lng = map['lng'];
    dataBean.addressId = map['address_id'];
    dataBean.address = map['address'];
    dataBean.products = List()..addAll(
      (map['products'] as List ?? []).map((o) => ProductsBean.fromMap(o))
    );
    dataBean.bankTransmissionReceipt = map['bank_transmission_receipt'];
    dataBean.bankName = map['bank_name'];
    dataBean.notes = map['notes'];
    dataBean.createdAt = map['created_at'];
    dataBean.updatedAt = map['updated_at'];
    return dataBean;
  }

  Map toJson() => {
    "id": id,
    "price_type": priceType,
    "pre_price": prePrice,
    "discount_type": discountType,
    "discount": discount,
    "tax1_type": tax1Type,
    "tax1": tax1,
    "tax2_type": tax2Type,
    "tax2": tax2,
    "total_price": totalPrice,
    "paid": paid,
    "rest": rest,
    "shipping_price": shippingPrice,
    "status": status,
    "email": email,
    "phone": phone,
    "payment_method": paymentMethod,
    "rate": rate,
    "report": report,
    "date": date,
    "name": name,
    "user_id": userId,
    "representative_id": representativeId,
    "representative_name": representativeName,
    "lat": lat,
    "lng": lng,
    "address_id": addressId,
    "address": address,
    "products": products,
    "bank_transmission_receipt": bankTransmissionReceipt,
    "bank_name": bankName,
    "notes": notes,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };
}
