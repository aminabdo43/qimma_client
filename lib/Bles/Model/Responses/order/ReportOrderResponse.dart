import 'package:qimma_client/utils/base/BaseResponse.dart';

class ReportOrderResponse extends BaseResponse {
  int status;
  String message;
  dynamic data;

  static ReportOrderResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    ReportOrderResponse reportOrderResponseBean = ReportOrderResponse();
    reportOrderResponseBean.status = map['status'];
    reportOrderResponseBean.message = map['message'];
    reportOrderResponseBean.data = map['data'];
    return reportOrderResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}