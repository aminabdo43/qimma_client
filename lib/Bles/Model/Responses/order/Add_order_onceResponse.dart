import 'package:qimma_client/Bles/Model/Responses/home/FilterWebSiteResponse.dart';
import 'package:qimma_client/utils/base/BaseResponse.dart';

class AddOrderOnceResponse extends BaseResponse{

  int status;
  DataBean data;
  String message;

  static AddOrderOnceResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    AddOrderOnceResponse addOrderOnceResponseBean = AddOrderOnceResponse();
    addOrderOnceResponseBean.status = map['status'];
    addOrderOnceResponseBean.data = DataBean.fromMap(map['data']);
    addOrderOnceResponseBean.message = map['message'];
    return addOrderOnceResponseBean;
  }

  Map toJson() => {
    "status": status,
    "data": data,
    "message": message,
  };

}

class DataBean {
  int id;
  dynamic priceType;
  dynamic discount;
  dynamic tax1;
  dynamic tax2;
  dynamic totalPrice;
  dynamic paid;
  dynamic rest;
  int shippingPrice;
  dynamic status;
  String email;
  String phone;
  String paymentMethod;
  dynamic rate;
  dynamic report;
  String date;
  String name;
  int userId;
  dynamic representativeId;
  String lat;
  String lng;
  String address;
  List<ProductsBean> products;
  String createdAt;
  String updatedAt;

  static DataBean fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    DataBean dataBean = DataBean();
    dataBean.id = map['id'];
    dataBean.priceType = map['price_type'];
    dataBean.discount = map['discount'];
    dataBean.tax1 = map['tax1'];
    dataBean.tax2 = map['tax2'];
    dataBean.totalPrice = map['total_price'];
    dataBean.paid = map['paid'];
    dataBean.rest = map['rest'];
    dataBean.shippingPrice = map['shipping_price'];
    dataBean.status = map['status'];
    dataBean.email = map['email'];
    dataBean.phone = map['phone'];
    dataBean.paymentMethod = map['payment_method'];
    dataBean.rate = map['rate'];
    dataBean.report = map['report'];
    dataBean.date = map['date'];
    dataBean.name = map['name'];
    dataBean.userId = map['user_id'];
    dataBean.representativeId = map['representative_id'];
    dataBean.lat = map['lat'];
    dataBean.lng = map['lng'];
    dataBean.address = map['address'];
    dataBean.products = List()..addAll(
      (map['products'] as List ?? []).map((o) => ProductsBean.fromMap(o))
    );
    dataBean.createdAt = map['created_at'];
    dataBean.updatedAt = map['updated_at'];
    return dataBean;
  }

  Map toJson() => {
    "id": id,
    "price_type": priceType,
    "discount": discount,
    "tax1": tax1,
    "tax2": tax2,
    "total_price": totalPrice,
    "paid": paid,
    "rest": rest,
    "shipping_price": shippingPrice,
    "status": status,
    "email": email,
    "phone": phone,
    "payment_method": paymentMethod,
    "rate": rate,
    "report": report,
    "date": date,
    "name": name,
    "user_id": userId,
    "representative_id": representativeId,
    "lat": lat,
    "lng": lng,
    "address": address,
    "products": products,
    "created_at": createdAt,
    "updated_at": updatedAt,
  };

}
