import 'package:qimma_client/utils/base/BaseResponse.dart';

class RateOrderResponse extends BaseResponse {
  int status;
  String message;
  dynamic data;

  static RateOrderResponse fromMap(Map<String, dynamic> map) {
    if (map == null) return null;
    RateOrderResponse rateOrderResponseBean = RateOrderResponse();
    rateOrderResponseBean.status = map['status'];
    rateOrderResponseBean.message = map['message'];
    rateOrderResponseBean.data = map['data'];
    return rateOrderResponseBean;
  }

  Map toJson() => {
    "status": status,
    "message": message,
    "data": data,
  };
}