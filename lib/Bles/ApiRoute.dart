
import 'package:qimma_client/utils/app_utils.dart';

class ApiRoutes {

  static String generalGet(String urlExtention) {
    return urlExtention;
  }

  // home

  static String home(){
    return "Home/home";
  }
  static String get_main_cat(){
    return "Home/get_main_cat";
  }
  static String filter_website(int cat_id){
    return "Home/filter_website?cat_id=$cat_id";
  }
  static String search_by_name(String name){
    return "Home/search_by_name?name=$name";
  }
  static String filter(String from,String to , String type ,String cat_id){
    return "Home/filter?from=$from&to=$to&type=$type&lat=1.2545484&lng=1.2541844&cat_id=$cat_id";
  }

  // add adress
  static String add_address(){
    return "Auth_private/add_address";
  }

  // Login
  static String login(){
    return "Auth_general/login";
  }

  static String login_social(String eamil){
    return "Auth_general/login_social";
  }


  // my cart
  static String my_cart(){
    return "Auth_private/my_cart";
  }
  static String count_of_cart(){
    return "Auth_private/count_of_cart";
  }
  static String delete_from_cart(int productID){
    return "Product/delete_from_cart/$productID";
  }
  static String update_cart(){
    return "Product/update_cart";
  }

  // product details
  static String get_products(int type){
    return "Product/get_products?type=1";
  }
  static String get_single_product(dynamic productID){
    return "Product/product_details/$productID";
  }

  static String add_to_wishlist(String productID){
    return "Product/wishlist/$productID";
  }

  // edit profile
  static String edit_profile(){
    return "Auth_private/edit_profile";
  }
  static String change_image(){
    return "Auth_private/save_image";
  }


  // notification
  static String get_notification(){
    return "Auth_private/get_notification";
  }


  // forget_password
  static String forget_password(int type){
    return "Auth_general/forget_password";
  }

  // show_profile
  static String my_info(){
    return "Auth_private/my_info";
  }
  static String my_order(){
    return "Order/my_order";
  }
  static String my_wishlist(){
    return "Auth_private/my_wishlist";
  }
  static String user_address(){
    return "Order/user_address";
  }
  static String logout(int page){
    return "Auth_private/logout";
  }

  // reset_password
  static String reset_password(){
    return "Auth_general/reset_password";
  }
  // products
  static String products_by_category({int catID , int page}){
    return "Product/products_by_category/$catID";
    // return "Product/products_by_category/$catID?page=$page";
  }
  // messages
  static String send_message(){
    return "message/send_message";
  }
  static String get_message(){
    return "message/get_message";
  }

  // registration
  static String register(){
    return "Auth_general/register";
  }
  static String register_social(){
    return "uth_general/register_social";
  }

  // add to cart
  static String add_to_cart(String productID){
    return "Product/add_to_cart/$productID";
  }


  // order
  static String add_order_once(){
    return "Order/add_order_once";
  }
  static String rate_order(int orderID){
    return "Order/rate_order/$orderID";
  }
  static String raport_order(int orderID){
    return "Order/raport_order/$orderID";
  }
  static String single_order(int orderID){
    return "Order/single_order/$orderID";
  }
  static String check_discount_code(){
    return "Order/check_discount_code";
  }
  static String remove_code(int orderID){
    return "Order/remove_code/$orderID";
  }
  static String my_orders(){
    return "Order/my_order";
  }
  static String rate_product(String productID){
    return "rate_comment/save_my_rate/$productID";
  }

  // filter

  static String mob_home_sorted_filter(){
    return "Home/mob_home_sorted_filter";
  }

  // edit order status
  static String editStatus(int orderID) {
    return "Order/edit_status/$orderID";
  }
}

class ApiRoutesUpdate {

  //static String baseUrl = "https://api.qimmajomla.com/api/";

  //static String baseUrl_client = "https://qimmaapi.codecaique.com/api/";
  static String baseUrl_client = "https://api.qimmajomla.com/api/";

  getLink(String url) {
    print("url ------>>>>   " + baseUrl_client + url);
    return baseUrl_client + url;
  }

}
