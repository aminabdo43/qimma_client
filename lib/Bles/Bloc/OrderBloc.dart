import 'package:qimma_client/Bles/Model/Requests/AddOrderRequest.dart';
import 'package:qimma_client/Bles/Model/Requests/EditStatusRequest.dart';
import 'package:qimma_client/Bles/Model/Responses/order/Add_order_onceResponse.dart';
import 'package:qimma_client/Bles/Model/Responses/order/EditStatusResponse.dart';
import 'package:qimma_client/Bles/Model/Responses/products/ProductByCat.dart';
import 'package:qimma_client/Bles/Model/Responses/products/SingleProduct.dart';
import 'package:qimma_client/utils/base/BaseBloc.dart';
import 'package:rxdart/rxdart.dart';

import '../ApiRoute.dart';

class OrderBloc extends BaseBloc {
  BehaviorSubject<AddOrderOnceResponse> _add_order =
      BehaviorSubject<AddOrderOnceResponse>();

  BehaviorSubject<EditStatusResponse>
  _edit_status =
  BehaviorSubject<EditStatusResponse>();

  addOrder(AddOrderRequest request) async {
    _add_order.value = AddOrderOnceResponse();
    _add_order.value.loading = true;
    AddOrderOnceResponse response = AddOrderOnceResponse.fromMap(
        (await repository.post(ApiRoutes.add_order_once(),(await request?.toJson())  ?? _add_order.value, isForm: true))
            ?.data) ;
    _add_order.value = response;
    _add_order.value.loading = false;
  }

  Future<EditStatusResponse> editStatus(
      EditStatusRequest request , int orderID) async {
    _edit_status.value = EditStatusResponse();
    _edit_status.value.loading = true;
    EditStatusResponse response =
    EditStatusResponse.fromMap((await repository.post(
        ApiRoutes.editStatus(orderID), request.toJson()))
        .data);
    _edit_status.value = response;
    _edit_status.value.loading = false;
    return response;
  }
  BehaviorSubject<AddOrderOnceResponse> get add_order => _add_order;
  BehaviorSubject<EditStatusResponse> get edit_status => _edit_status;
}

final orderBloc = OrderBloc();
