import 'package:qimma_client/Bles/Model/Responses/products/ProductByCat.dart';
import 'package:qimma_client/Bles/Model/Responses/products/SingleProduct.dart';
import 'package:qimma_client/utils/base/BaseBloc.dart';
import 'package:qimma_client/utils/base/BaseResponse.dart';
import 'package:rxdart/rxdart.dart';

import '../ApiRoute.dart';

class ProductBloc extends BaseBloc {

  BehaviorSubject<SingleProductResponse> _get_single_product = BehaviorSubject<SingleProductResponse>();
  BehaviorSubject<ProductByCatResponse> _get_products_by_cat = BehaviorSubject<ProductByCatResponse>();
  BehaviorSubject<BaseResponse> _add_wish_list = BehaviorSubject<BaseResponse>();
  BehaviorSubject<BaseResponse> _rate = BehaviorSubject<BaseResponse>();

  get_single_product(dynamic productID) async {
    _get_single_product.value = SingleProductResponse();
    _get_single_product.value.loading = true ;
    SingleProductResponse response = SingleProductResponse.fromMap((await repository.get(ApiRoutes.get_single_product(productID))).data);
    _get_single_product.value = response;
    _get_single_product.value.loading = false;
    _get_single_product.value = _get_single_product.value;
  }

  get_products_by_cat(int catID) async {
    _get_products_by_cat.value = ProductByCatResponse();
    _get_products_by_cat.value.loading = true ;
    ProductByCatResponse response = ProductByCatResponse.fromMap((await repository.get(ApiRoutes.products_by_category(catID: catID))).data);
    _get_products_by_cat.value = response;
    _get_products_by_cat.value.loading = false ;
    _get_products_by_cat.value = _get_products_by_cat.value;
  }


  Future<BaseResponse> add_wish_list(String  productID) async {
    _add_wish_list.value = BaseResponse();
    _add_wish_list.value.loading = true ;
    (await repository.post(ApiRoutes.add_to_wishlist(productID) , null));
    // _add_wish_list.value = response;
    _add_wish_list.value.loading = false ;
    return null;
  }

  Future<BaseResponse> rateProduct(String  productID,String rate ) async {

    await repository.post(ApiRoutes.rate_product(productID) , {"rate" : rate});
    // _add_wish_list.value = response;

    return null;
  }

  BehaviorSubject<SingleProductResponse> get single_product => _get_single_product;
  BehaviorSubject<ProductByCatResponse> get productsByCat => _get_products_by_cat;

}

final productBloc = ProductBloc();