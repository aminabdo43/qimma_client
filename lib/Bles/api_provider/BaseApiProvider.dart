import 'package:dio/dio.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/base/BasePostResponse.dart';
import 'package:qimma_client/utils/base/BaseRequest.dart';
import 'package:qimma_client/utils/base/BaseResponse.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '../ApiRoute.dart';

SharedPreferences pref;
Options get options => Options(
  followRedirects: false,
  validateStatus: (status) {
    return status < 500;
  },
  headers: {
    'content-type': 'application/x-www-form-urlencoded',
    'Authorization': AppUtils.userData == null ? '' : 'Bearer ${AppUtils.userData.token}',
    'lang' : pref.getString('langCode') ?? 'ar'
  },
);
Options get options2 => Options(
  followRedirects: false,
  validateStatus: (status) {
    return status < 500;
  },
  headers: {
    'content-type': 'application/x-www-form-urlencoded',
    'Authorization': AppUtils.userData == null ? '' : 'Bearer ${AppUtils.userData.token}',
    'lang' : pref.getString('langCode') ?? 'ar'
  },
);

class BaseApiProvider {
  String baseUrl = "";
  String token = "";

  d1io1() async{
    dio.interceptors.clear();
    pref = await SharedPreferences.getInstance();

    dio.interceptors.add(
      InterceptorsWrapper(
        onRequest: (RequestOptions options) {
          // Do something before request is sent
          //options.headers["Authorization"] = "Bearer " + token;
          options.headers["Authorization"] = token;
          return options;
        },
        onResponse: (Response response) {
          // Do something with response data
          return response; // continue
        },
        onError: (DioError error) async {
          // Do something with response error
          if (error.response?.statusCode == 403) {
            dio.interceptors.requestLock.lock();
            dio.interceptors.responseLock.lock();
            RequestOptions options = error.response.request;
            // FirebaseUser user = await FirebaseAuth.instance.currentUser();
            // token = await user.getIdToken(refresh: true);
            // await writeAuthKey(token);
            options.headers["Authorization"] = "Bearer " + token;

            dio.interceptors.requestLock.unlock();
            dio.interceptors.responseLock.unlock();
            return dio.request(options.path, options: options);
          } else {
            return error;
          }
        },
      ),
    );

    dio.options.baseUrl = baseUrl;
    return dio;
  }

  Dio dio = Dio();

  FormData formData;

  void printe(Response response) {
    if (response == null) {
      print("null response");
    } else {
      print("response message " + response.statusMessage);
      print("response message " + response.statusCode.toString());
    }
  }

  Future<Response> generalGet(String urlExtension) async {
    try {
      pref = await SharedPreferences.getInstance();
      if(urlExtension.contains("product_details")){
        Response response = await dio.get(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtension)),
          options: options2,
        );
        print("response ----- >");
        print(response);
        return response;
      }
      else{
        Response response = await dio.get(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtension)),
          options: options,
        );
        print("response ----- >");
        print(response);
        return response;
      }

    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      Response response = new Response(statusMessage: "no internet connection"
          , statusCode: 404 , data: BaseResponse(loading: false ,
              message: "connection error" , status: 0 ));
      AppUtils.showToast(msg: "Connection Error with Network");
      return response;
    }
  }

  Future<Response> generalDelete(String urlExtention) async {
    try {
      pref = await SharedPreferences.getInstance();
      Response response = await dio.delete(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtention)),
          options: options);

      print("response ----- >");
      print(response);
      return response;

    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }

  Future<Response> generalPost(String urlExtention, Map<dynamic, dynamic> request, {bool isForm = false}) async {
    try {
      pref = await SharedPreferences.getInstance();
      Response response = await dio.post(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtention)),
          options: options,
          data: isForm ? FormData.fromMap(request) : request,
      onSendProgress: (int sent, int total) {
        print("$sent $total");
      });
      print("response ----- >");
      print(response);
      return response;
    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }

  Future<Response> generalPostUpload(
      String urlExtention, FormData request) async {
    try {
      pref = await SharedPreferences.getInstance();
      Response response = await dio.post(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtention)),
          options: options,
          data: request,
      onSendProgress: (int sent, int total) {
        print("$sent $total");
      });
      print("response ----- >");
      print(response);
      return response;
    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }

  Future<Response> generalPut(
      String urlExtention, Map<dynamic, dynamic> request) async {
    try {
      pref = await SharedPreferences.getInstance();
      Response response = await dio.put(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtention)),
          options: options,
          data: request);
      print("response ----- >");
      print(response);
      return response;
    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }

  Future<BasePostResponse> generalPostObject(
      String urlExtention,BaseRequest request) async {
    try {
      pref = await SharedPreferences.getInstance();
      BasePostResponse response = await dio.post(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtention)),
          options: options,
          data: request);
      print("response ----- >");
      print(response);
      return response;
    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }

  Future<Dio> getApiClient() async {
    String token = "await storage.read(key: USER_TOKEN)";
    pref = await SharedPreferences.getInstance();
    dio.interceptors.clear();
    dio.interceptors
        .add(InterceptorsWrapper(onRequest: (RequestOptions options) {
      // Do something before request is sent
      options.headers["Authorization"] = "Bearer " + token;
      return options;
    }, onResponse: (Response response) {
      // Do something with response data
      print("response ----- >");
      print(response);
      return response; // continue
    }, onError: (DioError error) async {
      // Do something with response error
      if (error.response?.statusCode == 403) {
        dio.interceptors.requestLock.lock();
        dio.interceptors.responseLock.lock();
        RequestOptions options = error.response.request;
        // FirebaseUser user = await FirebaseAuth.instance.currentUser();
        // token = await user.getIdToken(refresh: true);
        // await writeAuthKey(token);
        options.headers["Authorization"] = "Bearer " + token;

        dio.interceptors.requestLock.unlock();
        dio.interceptors.responseLock.unlock();
        return dio.request(options.path, options: options);
      } else {
        return error;
      }
    }));
    dio.options.baseUrl = baseUrl;
    return dio;
  }



  Future<Response> generalPatch(String urlExtention, Map<dynamic, dynamic> request) async {
    try {
      pref = await SharedPreferences.getInstance();
      Response response = await dio.patch(
          ApiRoutesUpdate().getLink(ApiRoutes.generalGet(urlExtention)),
          options: options,
          data: request).catchError((onError){

      });
      print("response ----- >");
      print(response);
      return response;
    } catch (error, stacktrace) {
      print("response 000 ");
      print("Exception occured: $error stackTrace: $stacktrace");
      return null;
    }
  }





}
