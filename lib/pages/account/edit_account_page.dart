import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/ProfileBloc.dart';
import 'package:qimma_client/Bles/Model/Requests/EditProfileRequest.dart';
import 'package:qimma_client/Bles/Model/Responses/profile/MyInfoResponse.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_button.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';
import 'package:qimma_client/widgets/my_loader.dart';

class EditAccountPage extends StatefulWidget {
  @override
  _EditAccountPageState createState() => _EditAccountPageState();
}

class _EditAccountPageState extends State<EditAccountPage> {
  TextEditingController firstNameController = TextEditingController();
  TextEditingController lastNameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();

  bool gotData = false;

  File selectedFile;

  @override
  void initState() {
    super.initState();

    if (profileBloc.s_my_info.value == null ||
        profileBloc.s_my_info.value.data == null
    || profileBloc.s_my_info == null) {
      profileBloc.my_info();
    }
  }

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(14)),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: AppUtils.translate(context, 'account_settings'),
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              Stack(
                clipBehavior: Clip.none,
                children: [
                  Container(
                    width: size.width * 5,
                    height: size.width * .4,
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: Image.asset('assets/images/circle.png').image,
                      ),
                    ),
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.all(screenUtil.setWidth(8.0)),
                        child: CircleAvatar(
                          radius: 50,
                          backgroundImage: selectedFile != null ? Image.file(selectedFile).image : AppUtils.userData.image == null
                              ? Image.asset('assets/images/avatar.jpg').image
                              : CachedNetworkImageProvider(
                                  AppUtils.userData.image,
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: screenUtil.setHeight(20),
                    right: screenUtil.setWidth(95),
                    child: MyCircleButton(
                      color: Colors.white,
                      onTap: () async {
                        bool granted = await AppUtils.askPhotosPermission();
                        if(granted) {
                          List<File> image = await AppUtils.getImage(1);
                          setState(() {
                            selectedFile = image[0];
                          });
                        } else {
                          // user denied the permission
                        }
                      },
                      elevation: 8,
                      child: Icon(
                        Icons.camera_alt,
                        color: mainColor,
                      ),
                    ),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              StreamBuilder<MyInfoResponse>(
                stream: profileBloc.s_my_info.stream,
                builder: (context, snapshot) {
                  if(profileBloc.s_my_info.value.loading ) {
                    return Column(
                      children: [
                        SizedBox(height: size.width / 2,),
                        Loader(),
                        SizedBox(height: size.width / 2,),
                      ],
                    );
                  } else {
                    if(!gotData) {
                      firstNameController.text = profileBloc.s_my_info.value.data.firstName;
                      lastNameController.text = profileBloc.s_my_info.value.data.lastName;
                      phoneController.text = profileBloc.s_my_info.value.data.phone;
                      emailController.text = profileBloc.s_my_info.value.data.email;
                      gotData = true;
                    }
                    return Column(
                      children: [
                        Row(
                          children: [
                            Expanded(
                              child: TextFormField(
                                controller: firstNameController,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: Colors.grey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: secondColor),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: Colors.red),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: Colors.grey),
                                  ),
                                  labelText: AppUtils.translate(context, 'first_name'),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: screenUtil.setWidth(18),
                            ),
                            Expanded(
                              child: TextFormField(
                                controller: lastNameController,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  enabledBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: Colors.grey),
                                  ),
                                  focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: secondColor),
                                  ),
                                  errorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: Colors.red),
                                  ),
                                  focusedErrorBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5),
                                    borderSide: BorderSide(color: Colors.grey),
                                  ),
                                  labelText: AppUtils.translate(context, 'last_name'),
                                ),
                              ),
                            ),
                          ],
                        ),
                        SizedBox(
                          height: screenUtil.setHeight(18),
                        ),
                        TextFormField(
                          controller: phoneController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: secondColor),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            labelText: AppUtils.translate(context, 'phone_number'),
                          ),
                          keyboardType: TextInputType.phone,
                        ),
                        SizedBox(
                          height: screenUtil.setHeight(18),
                        ),
                        TextFormField(
                          controller: emailController,
                          decoration: InputDecoration(
                            border: InputBorder.none,
                            enabledBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            focusedBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: secondColor),
                            ),
                            errorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.red),
                            ),
                            focusedErrorBorder: OutlineInputBorder(
                              borderRadius: BorderRadius.circular(5),
                              borderSide: BorderSide(color: Colors.grey),
                            ),
                            labelText: AppUtils.translate(context, 'email'),
                          ),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).padding.top +
                              screenUtil.setHeight(15),
                        ),
                        MyButton(
                            AppUtils.translate(context, 'save'),
                            onTap: () {
                              profileBloc.editProfile(EditProfileRequest
                                (firstName: firstNameController.text,
                                lastName: lastNameController.text,
                                email: emailController.text,
                                phone: phoneController.text,
                                image: selectedFile ?? null
                              ));
                            },
                            width: size.width * .7),
                        SizedBox(
                          height: MediaQuery.of(context).padding.top,
                        ),
                      ],
                    );
                  }
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
