
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';
import 'package:qimma_client/pages/about/contact_us_page.dart';
import 'package:qimma_client/pages/account/edit_account_page.dart';
import 'package:qimma_client/pages/address/addresses_page.dart';
import 'package:qimma_client/pages/contact_us/contact_us_page.dart';
import 'package:qimma_client/pages/home/home_content.dart';
import 'package:qimma_client/pages/orders/my_orders_page.dart';
import 'package:qimma_client/pages/settings/settings_page.dart';
import 'package:qimma_client/pages/welcome/welcome_page.dart';
import 'package:qimma_client/pages/wishlist/wishlist_page.dart';
import 'package:qimma_client/providers/page_provider.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';

class AccountPage extends StatelessWidget {

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: '',
                onBackBtnPressed: () {
                  Provider.of<PageProvider>(context, listen: false)
                      .setPage(0, HomeContent());
                },
                actions: [
                  MyCircleButton(
                    child: Image.asset('assets/images/logout.png'),
                    onTap: () async{
                      await AppUtils.saveUserData(null);
                      Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (_) => WelcomePage()),
                          (route) => false);
                    },
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              Container(
                width: size.width * 5,
                height: size.width * .4,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: Image.asset('assets/images/circle.png').image,
                  ),
                ),
                child: Padding(
                  padding: EdgeInsets.all(15.0),
                  child: CircleAvatar(
                    radius: 15,
                    backgroundImage: AppUtils.userData.image == null || AppUtils.userData.image.isEmpty ?
                    Image.asset('assets/images/avatar.jpg').image :
                    CachedNetworkImageProvider(AppUtils.userData.image),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top - screenUtil.setHeight(15.0),
              ),
              Text(
                '${AppUtils.userData.firstName} ${AppUtils.userData.lastName}',
                style: TextStyle(fontSize: screenUtil.setSp(16)),
              ),
              Text(
                AppUtils.userData.email,
                style: TextStyle(color: Colors.grey),
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),

              ListTile(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => EditAccountPage()),
                  );
                },
                leading: Image.asset('assets/images/settings.png'),
                title: Text(
                  AppUtils.translate(context, 'account_settings'),
                ),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => AddressesPage()),
                  );
                },
                leading: Image.asset('assets/images/address.png'),
                title: Text(AppUtils.translate(context, 'my_address')),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (_) => MyOrdersPage()),
                  );
                },
                leading: Image.asset('assets/images/orders.png'),
                title: Text(AppUtils.translate(context, 'my_orders')),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => WishlistPage(),
                    ),
                  );
                },
                leading: Image.asset('assets/images/wishlist.png'),
                title: Text(AppUtils.translate(context, 'wishlist')),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => SettingsPage(),
                    ),
                  );
                },
                leading: Image.asset('assets/images/global.png'),
                title: Text(AppUtils.translate(context, 'settings')),
              ),
              ListTile(
                onTap: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(
                      builder: (_) => ContactUSPage(),
                    ),
                  );
                },
                leading: Image.asset('assets/images/global.png'),
                title: Text(AppUtils.translate(context, 'contact_us')),
              ),

              SizedBox(
                height: MediaQuery.of(context).padding.top * 2,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
