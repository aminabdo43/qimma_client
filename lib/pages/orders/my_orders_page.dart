import 'package:cached_network_image/cached_network_image.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/OrderBloc.dart';
import 'package:qimma_client/Bles/Bloc/ProfileBloc.dart';
import 'package:qimma_client/Bles/Model/Requests/EditStatusRequest.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/Bles/Model/Responses/profile/MyOrderResponse.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_button.dart';
import 'package:qimma_client/widgets/my_loader.dart';

class MyOrdersPage extends StatefulWidget {
  @override
  _MyOrdersPageState createState() => _MyOrdersPageState();
}

class _MyOrdersPageState extends State<MyOrdersPage> {
  final ScreenUtil screenUtil = ScreenUtil();

  @override
  void initState() {
    super.initState();

    profileBloc.my_order();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.grey,
      body: Column(
        children: [
          Container(
            color: Colors.grey,
            child: Padding(
              padding: EdgeInsets.all(screenUtil.setWidth(14)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).padding.top,
                  ),
                  MyAppBar(
                    text: AppUtils.translate(context, 'my_orders'),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).padding.top,
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<MyOrderResponse>(
              stream: profileBloc.s_my_order.stream,
              builder: (context, snapshot) {
                if (profileBloc.s_my_order.value.loading) {
                  return Column(
                    children: [
                      SizedBox(
                        height: MediaQuery.of(context).size.width / 2,
                      ),
                      Loader(),
                      SizedBox(
                        height: MediaQuery.of(context).size.width / 2,
                      ),
                    ],
                  );
                } else {
                  return ListView.separated(
                    shrinkWrap: true,
                    physics: const AlwaysScrollableScrollPhysics(),
                    // physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      return OrderItem(order: snapshot.data.data[index]);
                    },
                    separatorBuilder: (context, index) {
                      return SizedBox(
                        height: 5,
                      );
                    },
                    itemCount: snapshot.data.data.length,
                  );
                }
              },
            )
          )


        ],
      ),
    );

  }


}

class OrderItem extends StatelessWidget {
  final MyOrdersModel order;

  const OrderItem({Key key, this.order}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        // Navigator.of(context).push(
        //   MaterialPageRoute(
        //     builder: (_) => OrderDetails(
        //       id: order.id,
        //     ),
        //   ),
        // );
      },
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
        child: Material(
          elevation: 4,
          borderRadius: BorderRadius.circular(12),
          child: Padding(
            padding: EdgeInsets.all(10.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Expanded(
                      child: Row(
                        children: [
                          Container(
                            height: 35,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              color: secondColor,
                            ),
                            child: Center(
                              child: Padding(
                                padding: EdgeInsets.symmetric(horizontal: 5),
                                child: Text(
                                  AppUtils.translate(context, 'delivery'),
                                  style: TextStyle(
                                    fontSize: 13,
                                    color: order.paymentMethod == 'online'
                                        ? mainColor
                                        : Colors.black,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                          SizedBox(
                            width: 8,
                          ),
                          if(order.status.contains("ملغ") ||order.status.contains("canc"))
                          Container(
                            height: 35,
                            width: size.width / 4,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: redColor),
                              color: redColor,
                            ),
                            child: Center(
                              child: Text(
                                order.status ?? '',
                                style: TextStyle(
                                  color: secondColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          )
                          else Container(
                            height: 35,
                            width: size.width / 4,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(5),
                              border: Border.all(color: mainColor),
                              color: mainColor,
                            ),
                            child: Center(
                              child: Text(
                                order.status ?? '',
                                style: TextStyle(
                                  color: secondColor,
                                  fontSize: 13,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Expanded(
                      child: Row(
                        children: [
                          Image.asset('assets/images/time.png'),
                          SizedBox(
                            width: 5,
                          ),
                          Text(
                            '${AppUtils.calcDate(context, order.createdAt)}',
                            overflow: TextOverflow.ellipsis,
                            style: TextStyle(color: Colors.black45),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Text(
                      '${AppUtils.translate(context, 'id')}: ',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      order.id.toString(),
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                order.products.isEmpty
                    ? SizedBox.shrink()
                    : SizedBox(
                  height: 10,
                ),
                order.products.isEmpty
                    ? SizedBox.shrink()
                    : Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      '${AppUtils.translate(context, 'items')}: ',
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      AppUtils.translate(context, 'show_products_quantity'),
                      style: TextStyle(color: Colors.grey),
                    ),
                    Text(
                      AppUtils.translate(context, 'price'),
                      style: TextStyle(color: Colors.grey),
                    ),
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                order.products.isEmpty
                    ? SizedBox.shrink()
                    : ListView.separated(
                  shrinkWrap: true,
                  physics: NeverScrollableScrollPhysics(),
                  itemBuilder: (context, index) {
                    return Item(
                      item: order.products[index],
                    );
                  },
                  itemCount: order.products.length,
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      height: 8,
                    );
                  },
                ),
                SizedBox(
                  height: 18,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(AppUtils.translate(context, '${AppUtils.translate(context, "subtotal")}')),
                    Text(AppUtils.translate(context, '${order.prePrice} ${AppUtils.translate(context, "eg")}')),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(AppUtils.translate(context, '${AppUtils.translate(context, "taxes")}')),
                    Text(AppUtils.translate(context, '${order.tax1}  %')),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(AppUtils.translate(context, '${AppUtils.translate(context, "delivery")}')),
                    Text(AppUtils.translate(context, '${order.shippingPrice}  ${AppUtils.translate(context, "eg")}')),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(AppUtils.translate(context, '${AppUtils.translate(context, "total")}')),
                    Text(AppUtils.translate(context, '${order.totalPrice}  ${AppUtils.translate(context, "eg")}')),
                  ],
                ),
                if(orderBloc.edit_status.value?.loading == true)
                  Loader()
                else if(order.status.contains("بإنتظار الدفع") ||order.status.contains("waiting"))
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      MaterialButton(onPressed: (){
                        orderBloc.editStatus(EditStatusRequest(status: 8) , order.id).then((value) {
                          AppUtils.showToast(msg: value.message , bgColor: redColor);
                        });
                      },
                        color: redColor,
                        textColor: Colors.white ,
                        child: Text("${AppUtils.translate(context, 'cancel')}"),
                      )
                    ],
                  ),
              ],
            ),

          ),
        ),
      ),
    );
  }
}

class Item extends StatelessWidget {
  final ProductModel item;

  const Item({Key key, this.item}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Expanded(
          child: Row(
            children: [
              if (item?.image == null) CachedNetworkImage(
                imageUrl: "",
                width: 35,
                errorWidget: (_, __, ___) {
                  return Image.asset('assets/images/no_image.png');
                },
                height: 35,
              ) else CachedNetworkImage(
                imageUrl: item?.image,
                width: 35,
                errorWidget: (_, __, ___) {
                  return Image.asset('assets/images/no_image.png');
                },
                height: 35,
              ),
              SizedBox(
                width: 8,
              ),
              Flexible(
                child: Text(
                  '${item?.Difference}',
                  style: TextStyle(color: Colors.grey),
                ),
              ),
            ],
          ),
        ),
        Text(
          '${item.quantity}  X   ',
          style: TextStyle(color: Colors.deepOrangeAccent),
        ),
        Text(
          '${item.price} ${AppUtils.translate(context, 'eg')}',
          textAlign: TextAlign.right,
          style: TextStyle(color: Colors.red),
        ),
      ],
    );
  }
}


