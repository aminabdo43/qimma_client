import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/ProfileBloc.dart';
import 'package:qimma_client/Bles/Model/Responses/profile/MyWishListResponse.dart';
import 'package:qimma_client/pages/account/edit_account_page.dart';
import 'package:qimma_client/pages/home/single_product_details_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';
import 'package:qimma_client/widgets/my_loader.dart';
import 'package:qimma_client/widgets/my_sliver_grid_delegate.dart';
import 'package:qimma_client/widgets/my_text_form_field.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfileBState createState() => _ProfileBState();
}

class _ProfileBState extends State<ProfilePage> {
  int selectedTap = 0;

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  void initState() {
    super.initState();

    profileBloc.my_wishlist();
    profileBloc.my_info();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        width: size.width,
        height: size.height,
        decoration: BoxDecoration(
          image: DecorationImage(
            image: Image.asset('assets/images/profile.png').image,
            fit: BoxFit.fill,
          ),
        ),
        child: SingleChildScrollView(
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top * 2,
              ),
              MyAppBar(
                text: '',
                actions: [
                  MyCircleButton(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (_) => EditAccountPage(),),);
                    },
                    child: Icon(Icons.edit, color: Colors.black, size: 20,),
                  ),
                ],
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top + screenUtil.setHeight(15),
              ),
              CircleAvatar(
                radius: 60,
                backgroundImage: AppUtils.userData.image == null ? Image.asset('assets/images/avatar.jpg').image : CachedNetworkImageProvider(AppUtils.userData.image),
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
                 Padding(
                      padding: EdgeInsets.all(screenUtil.setWidth(8.0)),
                      child: StreamBuilder<MyWishListResponse>(
                          stream: profileBloc.s_my_wishlist.stream,
                          builder: (context, snapshot) {
                            if(profileBloc.s_my_wishlist.value.loading) {
                              return Column(
                                children: [
                                  SizedBox(height: size.width * .5,),
                                  Loader(),
                                  SizedBox(height: size.width * .5,),
                                ],
                              );
                            } else {
                              return Column(
                                children: [
                                  Row(
                                    children: [
                                      Expanded(child: MyTextFormField(
                                        enable: false,
                                        hintText: '${AppUtils.userData.firstName}',
                                        ),
                                      ),
                                      SizedBox(width: screenUtil.setWidth(15),),
                                      Expanded(child: MyTextFormField(
                                        enable: false,
                                        hintText: '${AppUtils.userData.lastName}',
                                        ),
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: screenUtil.setWidth(15),),
                                  MyTextFormField(
                                    enable: false,
                                    hintText: '${AppUtils.userData.email}',
                                  ),
                                  SizedBox(height: screenUtil.setWidth(15),),
                                  MyTextFormField(
                                    enable: false,
                                    hintText: '${AppUtils.userData.phone}',
                                  ),
                                  SizedBox(height: size.width * .2,),
                                  snapshot.data.data.isEmpty ? Text('No Products') : GridView.builder(
                                    shrinkWrap: true,
                                    physics: NeverScrollableScrollPhysics(),
                                    gridDelegate: MySliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
                                      crossAxisCount: 2,
                                      crossAxisSpacing: 8,
                                      mainAxisSpacing: 8,
                                      height: size.width * .7,
                                    ),
                                    itemBuilder: (context, index) {
                                      return GestureDetector(
                                        onTap: () {
                                          Navigator.of(context).push(
                                            MaterialPageRoute(
                                              builder: (_) => SingleProductDetailsPage(
                                                title: snapshot.data.data[index].Difference,
                                                productId: snapshot.data.data[index].mainProductId,
                                              ),
                                            ),
                                          );
                                        },
                                        child: Container(
                                          width: size.width / 2,
                                          child: Stack(
                                            children: [
                                              Container(
                                                width: double.infinity,
                                                height: size.width * .7,
                                                decoration: BoxDecoration(
                                                  borderRadius: BorderRadius.circular(18),
                                                  image: DecorationImage(
                                                    image: CachedNetworkImageProvider(snapshot.data.data[index].image),
                                                    fit: BoxFit.fill,
                                                  ),
                                                ),
                                              ),
                                              Positioned(
                                                left: screenUtil.setWidth(5),
                                                right: screenUtil.setWidth(5),
                                                bottom: screenUtil.setHeight(5),
                                                child: Material(
                                                  borderRadius: BorderRadius.circular(10),
                                                  color: Colors.white,
                                                  elevation: 4,
                                                  child: Padding(
                                                    padding: EdgeInsets.all(screenUtil.setWidth(4)),
                                                    child: Column(
                                                      crossAxisAlignment:
                                                      CrossAxisAlignment.start,
                                                      children: [
                                                        Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment.spaceBetween,
                                                          children: [
                                                            Flexible(
                                                              child: Text(
                                                                snapshot.data.data[index].Difference,
                                                                style: TextStyle(
                                                                  fontSize: screenUtil.setSp(12),
                                                                  fontWeight: FontWeight.bold,
                                                                ),
                                                              ),
                                                            ),
                                                            Text(
                                                              "${snapshot.data.data[index].SellingPrice} ${AppUtils.translate(context, "eg")}",
                                                              style: TextStyle(
                                                                fontSize: screenUtil.setSp(12),
                                                                fontWeight: FontWeight.bold,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                        Text(
                                                          snapshot.data.data[index].desc,
                                                          style: TextStyle(
                                                            color: Colors.grey,
                                                            fontSize: screenUtil.setSp(10),
                                                          ),
                                                        ),
                                                        Divider(),
                                                        Row(
                                                          mainAxisAlignment:
                                                          MainAxisAlignment.spaceEvenly,
                                                          children: [
                                                            MyCircleButton(
                                                              child: Image.asset(
                                                                'assets/images/cart_img.png',
                                                              ),
                                                            ),
                                                            MyCircleButton(
                                                              child: Image.asset(
                                                                'assets/images/toggle_img.png',
                                                              ),
                                                            ),
                                                            MyCircleButton(
                                                              child: Icon(
                                                                Icons.favorite,
                                                                size: screenUtil.setSp(18),
                                                                color: mainColor,
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ],
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      );
                                    },
                                    itemCount: snapshot.data.data.length,
                                  ),
                                ],
                              );
                            }
                          }
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }
}
