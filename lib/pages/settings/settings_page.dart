import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/main.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingsPage extends StatefulWidget {
  @override
  _SettingsPageState createState() => _SettingsPageState();
}

class _SettingsPageState extends State<SettingsPage> {
  SharedPreferences pref;

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  initState() {
    super.initState();

    initializePref();
  }

  // 0 => english  ||  1 => arabic
  int language = 0;


  // 0 => egp  ||  1 => ksa
  int country = 0;

  void onLanguageSelected(int val) {
    language = val;
    setState(() {});
    if (val == 0) {
      MyApp.setLocale(context, Locale('en'));
      pref.setString('langCode', 'en');
    } else {
      MyApp.setLocale(context, Locale('ar'));
      pref.setString('langCode', 'ar');
    }

    setState(() {});
  }

  void onCountrySelected(int val) {
    country = val;
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: AppUtils.translate(context, 'settings'),
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              Text(
                AppUtils.translate(context, 'language'),
                style: TextStyle(color: mainColor, fontSize: screenUtil.setSp(18)),
              ),
              RadioListTile(
                value: 0,
                groupValue: language,
                title: Text('English'),
                onChanged: onLanguageSelected,
              ),
              RadioListTile(
                value: 1,
                groupValue: language,
                title: Text('عربي'),
                onChanged: onLanguageSelected,
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              Text(
                'البلد',
                style: TextStyle(color: mainColor, fontSize: screenUtil.setSp(18)),
              ),
              RadioListTile(
                value: 0,
                groupValue: country,
                title: Text(AppUtils.translate(context, 'egypt')),
                onChanged: onCountrySelected,
              ),
              RadioListTile(
                value: 1,
                title: Text(AppUtils.translate(context, 'ksa')),
                groupValue: country,
                onChanged: onCountrySelected,
              ),
            ],
          ),
        ),
      ),
    );
  }

  void initializePref() async {
    pref = await SharedPreferences.getInstance();
    if (pref.getString('langCode') == null ||
        pref.getString('langCode') == 'en') {
      language = 0;
    } else {
      language = 1;
    }
    setState(() {});
  }
}
