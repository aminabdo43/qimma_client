
import 'package:flutter/material.dart';
import 'package:flutter_phone_direct_caller/flutter_phone_direct_caller.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/widgets/signup_background_image.dart';
import 'package:qimma_client/widgets/welcome_background_image.dart';

class ContactUSPage extends StatefulWidget {
  @override
  _ContactUSPageState createState() => _ContactUSPageState();
}
final ScreenUtil screenUtil = ScreenUtil();
class _ContactUSPageState extends State<ContactUSPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("${AppUtils.translate(context, 'contact_us')}"),
        //flexibleSpace: header(),
        leading: Container(),
      ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(0),
          child: Stack(
            children: [
              WelcomeBackgroundImage(),
              Padding(
                padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
                child: Column(
                  children: [
                    Text("قمة الجملة"),
                    SizedBox(
                      height: 10,
                    ),
                    Text("""تطبيق قمة الجملة يعتبر تطبيق قمة الجمله تطبيق رائد في مجال التجارة الالكترونيه بافكار جديده ومتميزه"""),
                    SizedBox(
                      height: 30,
                    ),
                    Text("0566761934"),
                    SizedBox(
                      height: 10,
                    ),
                    Text("0566761934"),
                    SizedBox(
                      height: 10,
                    ),
                    Text("0566761934"),
                    SizedBox(
                      height: 10,
                    ),
                    Text("0566761934"),
                    SizedBox(
                      height: 10,
                    ),
                    // Text(
                    //   Localizations.localeOf(context).languageCode == 'ar'
                    //       ? contentArabic
                    //       : contentArabic,
                    //   textDirection: TextDirection.rtl,
                    //   style: TextStyle(
                    //     fontSize: 18,
                    //     fontWeight: FontWeight.w600,
                    //
                    //   ),
                    // ),
                    FlatButton(
                        onPressed: () => _callNumber(),
                        child: new Text("اتصل بنا"),
                        color: Colors.green,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget header() {
    return Stack(
      children: [
        // Image.asset(
        //   "assets/images/logo.png",
        //   height: 60,
        //   fit: BoxFit.cover,
        //   width: MediaQuery.of(context).size.width,
        // ),
        Container(
          height: 60,
          child: Row(
            children: [
              SizedBox(
                width: 15,
              ),
              // InkWell(
              //   onTap: () {
              //     Navigator.pop(context);
              //   },
              //   child: Localizations.localeOf(context).languageCode == 'en'
              //       ? Image.asset(
              //     "assets/moreScreenImages/back.png",
              //     height: 25,
              //     width: 22,
              //   )
              //       : RotatedBox(
              //     quarterTurns: 2,
              //     child: Image.asset(
              //       "assets/moreScreenImages/back.png",
              //       height: 25,
              //       width: 22,
              //     ),
              //   ),
              // ),
              SizedBox(
                width: 10,
              ),
              Text(
                AppUtils.translate(context, 'contact_us'),
                style: GoogleFonts.poppins(
                    fontWeight: FontWeight.w600, color: Colors.white, fontSize: 22),
              )
            ],
          ),
        ),
      ],
    );
  }

  String contentEnglish = """""";

  String contentArabic = """  تطبيق قمة الجملة """;

  _callNumber() async{
    const number = '0566761934'; //set the number here
    bool res = await FlutterPhoneDirectCaller.callNumber(number);
  }
}
