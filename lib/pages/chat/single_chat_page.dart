import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';
import 'dart:math' as math;

class SingleChatPage extends StatelessWidget {
  final ScreenUtil screenUtil = ScreenUtil();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: [
          Material(
            elevation: 6,
            child: Column(
              children: [
                SizedBox(
                  height: MediaQuery.of(context).padding.top +
                      screenUtil.setHeight(5),
                ),
                Padding(
                  padding: EdgeInsets.all(screenUtil.setWidth(8.0)),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      Row(
                        children: [
                          GestureDetector(
                            child: Icon(
                              Localizations.localeOf(context).languageCode ==
                                      'en'
                                  ? Icons.arrow_back
                                  : Icons.arrow_forward,
                              color: Colors.black,
                              size: 24,
                            ),
                            onTap: () {
                              Navigator.pop(context);
                            },
                          ),
                          SizedBox(
                            width: screenUtil.setWidth(8),
                          ),
                          CircleAvatar(
                            backgroundImage:
                                Image.asset('assets/images/avatar.png').image,
                          ),
                          SizedBox(
                            width: screenUtil.setWidth(8),
                          ),
                          Text(
                            'احمد جمال',
                            style: TextStyle(fontSize: screenUtil.setSp(16)),
                          ),
                        ],
                      ),
                      MyCircleButton(
                        child: Icon(
                          Icons.phone,
                          color: mainColor,
                          size: 20,
                        ),
                        onTap: () {},
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
          Flexible(
            child: ListView.separated(
              itemBuilder: (context, index) {
                return index % 2 == 0
                    ? myContainer(context)
                    : senderContainer(context);
              },
              itemCount: 4,
              separatorBuilder: (BuildContext context, int index) {
                return SizedBox(
                  height: screenUtil.setHeight(10),
                );
              },
            ),
          ),
          chatMessagesBox(context),
        ],
      ),
    );
  }

  Widget chatMessagesBox(BuildContext context) {
    return Stack(
      clipBehavior: Clip.none,
      children: [
        Container(
          padding: EdgeInsets.symmetric(horizontal: screenUtil.setWidth(12)),
          margin: EdgeInsets.symmetric(
              horizontal: screenUtil.setWidth(12),
              vertical: screenUtil.setHeight(10)),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(40),
            boxShadow: [
              BoxShadow(
                color: Colors.grey,
                blurRadius: 5,
              ),
            ],
          ),
          height: screenUtil.setHeight(75),
          width: MediaQuery.of(context).size.width,
          child: TextField(
            keyboardType: TextInputType.multiline,
            maxLines: 100,
            decoration: InputDecoration(
              border: InputBorder.none,
              contentPadding: EdgeInsets.only(
                top: screenUtil.setHeight(20),
                left: screenUtil.setWidth(8),
                right: screenUtil.setWidth(8),
              ),
              hintText: AppUtils.translate(context, 'type_a_message'),
              hintStyle: TextStyle(
                color: Colors.grey,
              ),
            ),
          ),
        ),
        Positioned(
          right: Localizations.localeOf(context).languageCode == 'en'
              ? screenUtil.setWidth(25)
              : null,
          left: Localizations.localeOf(context).languageCode == 'ar'
              ? screenUtil.setWidth(25)
              : null,
          top: -15,
          child: FloatingActionButton(
            onPressed: () {},
            elevation: 0,
            backgroundColor: mainColor,
            highlightElevation: 0,
            child: RotatedBox(
              quarterTurns:
                  Localizations.localeOf(context).languageCode == 'en' ? 4 : 3,
              child: Icon(
                FontAwesomeIcons.telegramPlane,
                color: Colors.white,
              ),
            ),
            heroTag: 'this a private hero tag',
          ),
        ),
      ],
    );
  }

  Widget myContainer(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: Localizations.localeOf(context).languageCode == 'ar'
            ? screenUtil.setWidth(10)
            : (screenUtil.setWidth(50)),
        right: Localizations.localeOf(context).languageCode == 'ar'
            ? (screenUtil.setWidth(50))
            : screenUtil.setWidth(10),
        bottom: (screenUtil.setWidth(8)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Stack(
            clipBehavior: Clip.none,
            children: [
              Container(
                padding: EdgeInsets.all(screenUtil.setWidth(14)),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  color: Color(0xffE5F0E6),
                ),
                child: Text(
                  ' حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.',
                  textAlign: TextAlign.center,
                ),
              ),
              Positioned(
                bottom: -screenUtil.setWidth(8),
                right: Localizations.localeOf(context).languageCode == 'en'
                    ? screenUtil.setWidth(8)
                    : MediaQuery.of(context).size.width * .75,
                child: CircleAvatar(
                  radius: 10,
                  backgroundImage:
                      Image.asset('assets/images/avatar.png').image,
                ),
              ),
            ],
          ),
          Text(
            '9:00 AM',
            style:
                TextStyle(color: Colors.grey, fontSize: screenUtil.setSp(12)),
          ),
        ],
      ),
    );
  }

  Widget sendArrow(BuildContext context) {
    return Localizations.localeOf(context).languageCode == 'en'
        ? Transform(
            alignment: Alignment.center,
            transform: Matrix4.rotationY(math.pi),
            child: Image.asset(
              'assets/images/send.png',
              fit: BoxFit.fill,
            ),
          )
        : Image.asset('assets/images/send.png');
  }

  senderContainer(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        right: Localizations.localeOf(context).languageCode == 'ar'
            ? screenUtil.setWidth(10)
            : (screenUtil.setWidth(50)),
        left: Localizations.localeOf(context).languageCode == 'ar'
            ? (screenUtil.setWidth(50))
            : screenUtil.setWidth(10),
        bottom: (screenUtil.setWidth(8)),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          Stack(
            clipBehavior: Clip.none,
            children: [
              Container(
                padding: EdgeInsets.all(screenUtil.setWidth(14)),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(18),
                  color: Color(0xffF5F5F5),
                ),
                child: Text(
                  ' حيث يمكنك أن تولد مثل هذا النص أو العديد من النصوص الأخرى إضافة إلى زيادة عدد الحروف التى يولدها التطبيق.',
                  textAlign: TextAlign.center,
                ),
              ),
              Positioned(
                bottom: -screenUtil.setWidth(8),
                left: Localizations.localeOf(context).languageCode == 'en'
                    ? screenUtil.setWidth(8)
                    : MediaQuery.of(context).size.width * .75,
                child: CircleAvatar(
                  radius: 10,
                  backgroundImage:
                      Image.asset('assets/images/avatar.png').image,
                ),
              ),
            ],
          ),
          Text(
            '9:00 AM',
            style:
                TextStyle(color: Colors.grey, fontSize: screenUtil.setSp(12)),
          ),
        ],
      ),
    );
  }
}
