import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';
import 'package:qimma_client/pages/chat/single_chat_page.dart';
import 'package:qimma_client/pages/home/home_content.dart';
import 'package:qimma_client/providers/page_provider.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';

class PeopleChatPage extends StatelessWidget {

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        Provider.of<PageProvider>(context, listen: false).setPage(0, HomeContent());
        return false;
      },
      child: Scaffold(
        body: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: AppUtils.translate(context, 'my_chats'),
                onBackBtnPressed: () {
                  Provider.of<PageProvider>(context, listen: false).setPage(0, HomeContent());
                },
              ),
              Flexible(
                child: ListView.separated(
                  itemCount: 4,
                  itemBuilder: (context, index) {
                    return ListTile(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (_) => SingleChatPage(),),);
                      },
                      leading: CircleAvatar(
                        backgroundImage:
                            Image.asset('assets/images/avatar.png').image,
                      ),
                      title: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text('احمد جمال'),
                          Text(
                            '9:00 AM',
                            style: TextStyle(fontSize: screenUtil.setSp(12)),
                          ),
                        ],
                      ),
                      subtitle: Text(
                        'هذا النص هو مثال لنص يمكن أن يستبدل في نفس المساحة',
                        style: TextStyle(color: Colors.grey, fontSize: 12),
                      ),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) {
                    return SizedBox(
                      height: screenUtil.setHeight(5),
                    );
                  },
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
