import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';
import 'package:qimma_client/pages/home/home_content.dart';
import 'package:qimma_client/pages/home/home_page.dart';
import 'package:qimma_client/providers/page_provider.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/widgets/my_button.dart';

class DeliverPage extends StatelessWidget {

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Padding(
        padding: EdgeInsets.all(screenUtil.setWidth(12)),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              AppUtils.translate(context, 'delivery_thanks_msg'),
              textAlign: TextAlign.center,
              style: TextStyle(
                color: Colors.black,
                fontSize: screenUtil.setSp(17),
              ),
            ),
            SizedBox(
              height: screenUtil.setHeight(25),
            ),
            Container(
              width: size.width * 9,
              height: size.width * .8,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: Image.asset('assets/images/circle.png').image,
                ),
              ),
              child: Center(
                child: Padding(
                  padding: EdgeInsets.all(screenUtil.setWidth(8.0)),
                  child: Image.asset(
                    'assets/images/delivery.png',
                  ),
                ),
              ),
            ),
            SizedBox(
              height: screenUtil.setHeight(45),
            ),
            MyButton(
              AppUtils.translate(context, 'main_page'),
              width: size.width / 1.5,
              onTap: () {
                Provider.of<PageProvider>(context, listen: false)
                    .setPage(0, HomeContent());
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (_) => HomePage(),
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
