import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:location/location.dart';
import 'package:qimma_client/Bles/Bloc/AddAddressBloc.dart';
import 'package:qimma_client/Bles/Model/Requests/AddAddressRequest.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_button.dart';
import 'package:qimma_client/widgets/my_button2.dart';
import 'package:qimma_client/widgets/my_loader.dart';
import 'package:qimma_client/widgets/my_text_form_field.dart';

class AddNewAddressPage extends StatefulWidget {
  @override
  _AddNewAddressPageState createState() => _AddNewAddressPageState();
}

class _AddNewAddressPageState extends State<AddNewAddressPage> {

  final ScreenUtil screenUtil = ScreenUtil();

  TextEditingController addressController = TextEditingController();

  LocationData _locationData;
  bool pickingLocation = false;

  final formKey = GlobalKey<FormState>();

  bool loading = false;

  @override
  void initState() {
    super.initState();

    print(AppUtils.userData.token);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: LoadingOverlay(
        isLoading: loading,
        progressIndicator: Loader(),
        color: Colors.white,
        opacity: .5,
        child: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
          child: Form(
            key: formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: MediaQuery.of(context).padding.top,
                ),
                MyAppBar(
                  text: AppUtils.translate(context, 'add_new_address'),
                ),
                SizedBox(
                  height: MediaQuery.of(context).padding.top * 2,
                ),
                MyTextFormField(
                  validator: (String input) {
                    if (input.isEmpty) {
                      return AppUtils.translate(context, 'required');
                    }
                  },
                  controller: addressController,
                  hintText:AppUtils.translate(context, 'address'),
                ),
                SizedBox(
                  height: screenUtil.setHeight(30),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('Location Coordinates'),
                        _locationData == null
                            ? SizedBox.shrink()
                            : Row(
                          children: [
                            Icon(
                              Icons.check_circle,
                              color: mainColor,
                              size: 10,
                            ),
                            SizedBox(
                              width: screenUtil.setWidth(3),
                            ),
                            Text(
                              'Location picked',
                              style: TextStyle(
                                color: mainColor,
                                fontSize: screenUtil.setSp(12),
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                    SizedBox(
                      width: screenUtil.setWidth(15),
                    ),
                    Expanded(
                      child: MyButton2(
                        pickingLocation
                            ? CircularProgressIndicator()
                            : Text(
                          'Pick Location',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        btnColor: Colors.blueGrey,
                        onTap: () async {

                          setState(() {
                            pickingLocation = true;
                          });

                          var locationPermissionGranted = await AppUtils.askLocationPermission();
                          if (locationPermissionGranted) {
                            Location location = new Location();

                            bool _serviceEnabled;
                            PermissionStatus _permissionGranted;

                            _serviceEnabled = await location.serviceEnabled();
                            if (!_serviceEnabled) {
                              _serviceEnabled = await location.requestService();
                              if (!_serviceEnabled) {
                                AppUtils.showToast(msg: 'Please open GPS');
                                setState(() {
                                  pickingLocation = false;
                                });
                                return;
                              }
                            }

                            _permissionGranted = await location.hasPermission();
                            if (_permissionGranted == PermissionStatus.denied) {
                              _permissionGranted = await location.requestPermission();
                              if (_permissionGranted != PermissionStatus.granted) {
                                AppUtils.showToast(msg: 'Location permission is denied');
                                setState(() {
                                  pickingLocation = false;
                                });
                                return;
                              }
                            }

                            _locationData = await location.getLocation();
                            setState(() {
                              pickingLocation = false;
                            });
                          }
                        },
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: screenUtil.setHeight(30),
                ),
                Expanded(
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: MyButton(
                      AppUtils.translate(context, 'save'),
                      width: MediaQuery.of(context).size.width * .6,
                      onTap: () {
                        makeNewAddress(context);
                      },
                    ),
                  ),
                ),
                SizedBox(
                  height: screenUtil.setHeight(30),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void makeNewAddress(BuildContext context) async {
    if(formKey.currentState.validate()) {
      if (_locationData == null) {
        AppUtils.showToast(msg: AppUtils.translate(context, 'please_provide_the_location_coordinates'));
        return;
      }

      setState(() {
        loading = true;
      });

      var response = await addressBloc.add_address(AddAddressRequest(
        address: addressController.text,
        lat: _locationData.latitude.toString(),
        lng: _locationData.longitude.toString(),
      ));

      if(response.status >= 200 && response.status < 300) {
        setState(() {
          loading = false;
        });
        AppUtils.showToast(msg: AppUtils.translate(context, 'done'), bgColor: mainColor);
        Navigator.pop(context);
      } else {
        AppUtils.showToast(msg: response.message);
        setState(() {
          loading = false;
        });
      }
    }
  }
}
