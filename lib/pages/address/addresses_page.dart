import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/ProfileBloc.dart';
import 'package:qimma_client/Bles/Model/Responses/profile/UserAddressResponse.dart';
import 'package:qimma_client/pages/address/add_new_address_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/clickable_text.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_loader.dart';

class AddressesPage extends StatefulWidget {
  @override
  _AddressesPageState createState() => _AddressesPageState();
}

class _AddressesPageState extends State<AddressesPage> {
  int selected = 0;

  void onItemSelected(int val) {
    selected = val;
    setState(() {});
  }

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  void initState() {
    super.initState();

    profileBloc.user_address();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(18)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: AppUtils.translate(context, 'my_address'),
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              StreamBuilder<UserAddressResponse>(
                stream: profileBloc.s_user_address.stream,
                builder: (context, snapshot) {
                  if(profileBloc.s_user_address.value.loading) {
                    return Column(
                      children: [
                        SizedBox(height: screenUtil.screenWidth / 2,),
                        Loader(),
                        SizedBox(height: screenUtil.screenWidth / 2,),
                      ],
                    );
                  } else {
                    return Wrap(
                      children: [
                        Column(
                          children: List.generate(
                            snapshot.data.data.length,
                                (index) => Padding(
                              padding: EdgeInsets.all(screenUtil.setWidth(8.0)),
                              child: Material(
                                elevation: 5,
                                borderRadius: BorderRadius.circular(20),
                                child: RadioListTile(
                                  value: index,
                                  groupValue: selected,
                                  onChanged: onItemSelected,
                                  title: Text(snapshot.data.data[index].name),
                                  subtitle: Text(snapshot.data.data[index].address),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 15,),
                        ClickableText(
                          text: AppUtils.translate(context, 'add_new_address'),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (_) => AddNewAddressPage(),
                              ),
                            );
                          },
                          textStyle: TextStyle(
                            fontSize: screenUtil.setSp(20),
                            color: mainColor,
                            decoration: TextDecoration.underline,
                          ),
                        ),
                      ],
                    );
                  }
                }
              ),
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
