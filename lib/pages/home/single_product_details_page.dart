import 'dart:ffi';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qimma_client/Bles/Bloc/CartBloc.dart';
import 'package:qimma_client/Bles/Bloc/ProductsBloc.dart';
import 'package:qimma_client/Bles/Model/Requests/AddToCartRequest.dart';
import 'package:qimma_client/Bles/Model/Responses/products/SingleProduct.dart';
import 'package:qimma_client/pages/cart/cart_page.dart';
import 'package:qimma_client/pages/home/most_selling.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_button2.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';
import 'package:qimma_client/widgets/my_loader.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';

class SingleProductDetailsPage extends StatefulWidget {
  final dynamic productId;
  final String title;

  const SingleProductDetailsPage(
      {Key key, @required this.productId, @required this.title})
      : assert(productId != null),
        assert(title != null),
        super(key: key);

  @override
  _SingleProductDetailsPageState createState() =>
      _SingleProductDetailsPageState();
}

class _SingleProductDetailsPageState extends State<SingleProductDetailsPage> {
  bool favourite = false;

  bool gotdata = false;
  bool loading = false;
  bool addToCart = false;

  @override
  void initState() {
    super.initState();

    productBloc.get_single_product(widget.productId);
  }

  final ScreenUtil screenUtil = ScreenUtil();

  int selectedSize;

  int selectedSizeId;

  int selectedColor;

  int selectedColorId;

  bool gotSize = false;
  bool gotColor = false;



  
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    


    return Scaffold(
      bottomNavigationBar: productBloc.single_product.value.data == null
          ? SizedBox.shrink()
          : Container(
              width: size.width,
              decoration: BoxDecoration(
                color: Colors.white,
                boxShadow: [
                  BoxShadow(color: Colors.grey, spreadRadius: 1, blurRadius: 9),
                ],
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(30),
                  topLeft: Radius.circular(30),
                ),
              ),
              height: screenUtil.setHeight(120),
              padding: EdgeInsets.all(18),
              child: Column(
                children: [
                  Container(
                    height: screenUtil.setHeight(6),
                    width: size.width / 3 - screenUtil.setWidth(20),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(30),
                      color: Colors.green,
                    ),
                  ),
                  SizedBox(
                    height: screenUtil.setHeight(15),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      addToCart
                          ? Loader(
                              size: 20,
                            )
                          : MyButton2(
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset(
                                    'assets/images/cart_img_white.png',
                                  ),
                                  SizedBox(
                                    width: screenUtil.setWidth(8),
                                  ),
                                  Text(
                                    '${AppUtils.translate(context, 'add_to_cart')}',
                                    style: TextStyle(color: Colors.white),
                                  ),
                                ],
                              ),
                              height: screenUtil.setHeight(50),
                              width: size.width / 3,
                              onTap: () async {
                                setState(() {
                                  addToCart = true;
                                });
                                var response = await cartBloc.addToCart(
                                  AddToCartRequest(
                                    quantity: '1',
                                    colorId: selectedColorId.toString(),
                                    productId: productBloc
                                        .single_product.value.data.id
                                        .toString(),
                                    sizeId: selectedSizeId.toString(),
                                  ),
                                );
                                AppUtils.showAlertDialog(
                                    context: context,
                                    continue_: (){
                                      Navigator.of(context).pop();
                                    },
                                    goToCart: (){
                                      Navigator.of(context).push(MaterialPageRoute(builder: (_) => CartPage()));
                                    },
                                    header: "",
                                    content: "هل تريد اكمال التسوق"
                                );
                                if (response.status == 0) {
                                  AppUtils.showToast(msg: response.message);
                                } else {
                                  AppUtils.showToast(
                                      msg: AppUtils.translate(context, 'done'),
                                      bgColor: mainColor);
                                }

                                setState(() {
                                  addToCart = false;
                                });
                              },
                            ),
                      Text(
                        '${productBloc.single_product.value.data.SellingPrice} ${AppUtils.translate(context, "eg")}',
                        style: TextStyle(
                          color: mainColor,
                          fontSize: screenUtil.setSp(25),
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: widget.title,
              ),
              StreamBuilder<SingleProductResponse>(
                  stream: productBloc.single_product.stream,
                  builder: (context, snapshot) {
                    if (!productBloc.single_product.value.loading && snapshot.data.data == null) {
                      return Center(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            SizedBox(
                              height: size.width * .5,
                            ),
                            Text('product not found'),
                          ],
                        ),
                      );
                    }
                    if (productBloc.single_product.value.loading ||
                        snapshot.data.data == null) {
                      return Column(
                        children: [
                          SizedBox(
                            height: size.width * .5,
                          ),
                          Loader(),
                          SizedBox(
                            height: size.width * .5,
                          ),
                        ],
                      );
                    } else {
                      if (!gotdata) {
                        gotdata = true;
                        favourite = snapshot.data.data.isFavorite;
                        Future.delayed(Duration(seconds: 1), () {
                          setState(() {});
                        });
                      }
                      return Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          SizedBox(
                            height: MediaQuery.of(context).padding.top,
                          ),
                          Stack(
                            children: [
                              Container(
                                width: double.infinity,
                                height: size.width,
                                child: CachedNetworkImage(
                                  imageUrl: snapshot.data.data.image,
                                  width: size.width * .85,
                                  height: size.width * .8,
                                  fit: BoxFit.fill,
                                  errorWidget: (_, __, ___) {
                                    return Image.asset(
                                      'assets/images/no_image.png',
                                      width: double.infinity,
                                      height: size.width * .8,
                                      fit: BoxFit.fill,
                                    );
                                  },
                                  placeholder: (_, __) {
                                    return Loader();
                                  },
                                ),
                                // child: ListView.builder(
                                //   scrollDirection: Axis.horizontal,
                                //   itemCount: snapshot.data.data.images.length,
                                //   itemBuilder: (context, index) {
                                //     return Container(
                                //       margin: EdgeInsets.only(
                                //         right: screenUtil.setWidth(3),
                                //         left: screenUtil.setWidth(3),
                                //       ),
                                //       child: CachedNetworkImage(
                                //         imageUrl: snapshot.data.data.image,
                                //         width: size.width * .85,
                                //         height: size.width * .8,
                                //         fit: BoxFit.fill,
                                //         errorWidget: (_, __, ___) {
                                //           return Image.asset(
                                //             'assets/images/no_image.png',
                                //             width: double.infinity,
                                //             height: size.width * .8,
                                //             fit: BoxFit.fill,
                                //           );
                                //         },
                                //         placeholder: (_, __) {
                                //           return Loader();
                                //         },
                                //       ),
                                //     );
                                //   },
                                // ),
                              ),
                              Positioned(
                                bottom: screenUtil.setHeight(10),
                                left: 0,
                                right: 0,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    MyCircleButton(
                                      color: Colors.white,
                                      child: Icon(
                                        Icons.favorite,
                                        color: favourite
                                            ? Colors.red
                                            : Colors.grey,
                                      ),
                                      onTap: () {
                                        setState(() {
                                          favourite = !favourite;
                                        });

                                        if(favourite) {
                                          productBloc.add_wish_list(snapshot.data.data.id.toString());
                                        }
                                        else{
                                          productBloc.add_wish_list(snapshot.data.data.id.toString());
                                        }
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          // SmoothStarRating(
                          //     allowHalfRating: false,
                          //     onRated: (v) {
                          //       print("rating value -> $v");
                          //     },
                          //     starCount: 5,
                          //     rating: snapshot.data.data.rate * 1.0,
                          //     size: 40.0,
                          //     isReadOnly:true,
                          //     color: Colors.green,
                          //     borderColor: Colors.green,
                          //     spacing:0.0,
                          // ),
                          RatingBar.builder(
                            initialRating: 3,
                            minRating: 1,
                            direction: Axis.horizontal,
                            allowHalfRating: false,
                            itemCount: 5,
                            itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                            itemBuilder: (context, _) => Icon(
                            Icons.star,
                            color: Colors.green,
                            ),
                            onRatingUpdate: (rating) {
                              productBloc.rateProduct(widget.productId.toString(), rating.toString());
                            },
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                snapshot.data.data.Difference,
                                style: TextStyle(fontSize: 15),
                              ),
                              Text(
                                '${snapshot.data.data.SellingPrice} ${AppUtils.translate(context, "eg")}',
                                style: TextStyle(
                                  color: mainColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ],
                          ),
                          Text(
                            snapshot.data.data.InventoryName,
                            style: TextStyle(
                              color: mainColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          Text(snapshot.data.data.desc),
                          SizedBox(
                            height: screenUtil.setHeight(10),
                          ),
                          Divider(
                            thickness: 2,
                          ),
                          SizedBox(
                            height: screenUtil.setHeight(18),
                          ),
                          Row(
                            children: [
                              Text('الحجم'),
                              SizedBox(
                                width: screenUtil.setWidth(18),
                              ),
                              Row(
                                children: List.generate(
                                    snapshot.data.data.sizes.length, (index) {
                                  if (!gotSize) {
                                    gotSize = true;
                                    selectedSizeId =
                                        snapshot.data.data.sizes[index].id;
                                  }
                                  return GestureDetector(
                                    onTap: () {
                                      selectedSize = index;
                                      selectedSizeId =
                                          snapshot.data.data.sizes[index].id;
                                      setState(() {});
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: screenUtil.setWidth(8),
                                      ),
                                      child: Text(
                                        snapshot.data.data.sizes[index].name,
                                        style: TextStyle(
                                            color: selectedSize == index
                                                ? mainColor
                                                : Colors.black,
                                            decoration:
                                                TextDecoration.underline),
                                      ),
                                    ),
                                  );
                                }),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenUtil.setHeight(18),
                          ),
                          Row(
                            children: [
                              Text('الالوان المتاحة'),
                              SizedBox(
                                width: screenUtil.setWidth(18),
                              ),
                              Row(
                                children: List.generate(
                                    snapshot.data.data.colors.length, (index) {
                                  if (!gotColor) {
                                    gotColor = true;
                                    selectedColorId =
                                        snapshot.data.data.colors[index].id;
                                  }
                                  return GestureDetector(
                                    onTap: () {
                                      selectedColor = index;
                                      selectedColorId =
                                          snapshot.data.data.colors[index].id;
                                      setState(() {});
                                    },
                                    child: Padding(
                                      padding: EdgeInsets.symmetric(
                                        horizontal: screenUtil.setWidth(8),
                                      ),
                                      child: CircleAvatar(
                                        backgroundColor: Color(int.parse(
                                            '0xff${snapshot.data.data.colors[index].colorCode}')),
                                        child: selectedColor == index
                                            ? Icon(Icons.check)
                                            : SizedBox.shrink(),
                                      ),
                                    ),
                                  );
                                }),
                              ),
                            ],
                          ),
                          SizedBox(
                            height: screenUtil.setHeight(22),
                          ),
                          Divider(
                            thickness: 2,
                          ),
                          SizedBox(
                            height: screenUtil.setHeight(10),
                          ),
                          MostSellingPart(),
                        ],
                      );
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
