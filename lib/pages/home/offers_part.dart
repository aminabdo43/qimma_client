
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/pages/home/single_product_details_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/clickable_text.dart';
import 'package:qimma_client/widgets/my_loader.dart';

import 'category_page.dart';

class OffersPart extends StatelessWidget {

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return homeBloc.s_home.value.data.Offers == null || homeBloc.s_home.value.data.Offers.isEmpty ? SizedBox.shrink() : Container(
      height: size.width * .85,
      width: size.width,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(screenUtil.setWidth(8.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppUtils.translate(context, 'offers'),
                  style: TextStyle(fontSize: screenUtil.setSp(16)),
                ),
                ClickableText(
                  text: AppUtils.translate(context, 'show_all'),
                  textStyle: TextStyle(fontSize: screenUtil.setSp(16), color: mainColor),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => CategoryPage(
                          title: AppUtils.translate(context, 'offers'),
                          categoryId: int.parse(homeBloc.s_home.value.data.Offers[0].mainProductId),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          Expanded(
            child: Container(
              width: size.width,
              child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemBuilder: (context, index) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (_) => SingleProductDetailsPage(
                          title: homeBloc.s_home.value.data.Offers[index].Difference,
                          productId: homeBloc.s_home.value.data.Offers[index].id),),);
                    },
                    child: Card(
                      elevation: 3,
                      child: Container(
                        width: size.width * .65,
                        child: Column(
                          children: [
                            Expanded(
                              child: CachedNetworkImage(imageUrl: homeBloc.s_home.value.data.Offers[index].image,
                                width: double.infinity,
                                height: size.width * .7,
                                fit: BoxFit.fill,
                                errorWidget: (_, __, ___) {
                                  return Image.asset('assets/images/no_image.png', width: double.infinity,
                                    height: size.width * .7,
                                    fit: BoxFit.fill,
                                  );
                                },
                                placeholder: (_, __) {
                                  return Loader();
                                },
                              ),
                            ),
                            Container(
                              width: double.infinity,
                              child: Padding(
                                padding: EdgeInsets.all(screenUtil.setWidth(8)),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      homeBloc.s_home.value.data.Offers[index].Difference,
                                      style: TextStyle(color: Colors.black),
                                    ),
                                    Text(
                                      "${homeBloc.s_home.value.data.Offers[index].SellingPrice.toString()} ${AppUtils.translate(context, "eg")}",
                                      style: TextStyle(
                                        color: Colors.black,
                                        fontSize: screenUtil.setSp(12),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                              decoration: BoxDecoration(
                                gradient: LinearGradient(
                                  begin: Alignment.topCenter,
                                  end: Alignment.bottomCenter,
                                  colors: [
                                    Color(0xfffefef),
                                    Colors.black12,
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                },
                itemCount: homeBloc.s_home.value.data.Offers.length,
              ),
            ),
          ),
        ],
      ),
    );
  }
}