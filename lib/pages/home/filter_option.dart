import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/Bles/Model/Responses/home/GetMainCat.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';

class FilterOption extends StatefulWidget {
  @override
  _FilterOptionState createState() => _FilterOptionState();
}

class _FilterOptionState extends State<FilterOption> {
  List<CatsModel> options;

  double value = 1;

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  Widget build(BuildContext context) {
    homeBloc.get_main_cat();
    options = homeBloc.s_get_main_cat.value.data;

    return Padding(
      padding: EdgeInsets.all(screenUtil.setWidth(18)),
      child: Wrap(
        children: [
          MyAppBar(
            text: AppUtils.translate(context, 'filter'),
            actions: [
              MyCircleButton(
                child: Icon(
                  Icons.check,
                  color: Colors.white,
                ),
                color: mainColor,
                onTap: () {
                  Navigator.pop(context);
                },
              ),
            ],
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return GestureDetector(
                child: Option(
                  text: options[index].name,
                ),
                onTap: (){

                  //homeBloc.homeFilterRequest.catId.add(options[index].id) ;
                },
              );
            },
            itemCount: options.length,
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: screenUtil.setWidth(18.0)),
            child: Text(AppUtils.translate(context, 'price'), style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold, fontSize: 16),),
          ),
          Row(
            children: [
              Text('0'),
              Expanded(
                child: Slider(
                  value: value,
                  activeColor: mainColor,
                  inactiveColor: secondColor,
                  label: '$value',
                  divisions: 4,
                  min: 0,
                  max: 1000,
                  onChanged: (double value) {
                    this.value = value;
                    setState(() {});
                  },
                ),
              ),
              Text('1000'),
            ],
          ),
        ],
      ),
    );
  }
}

class Option extends StatefulWidget {

  final String text;
  final int id ;
  final Function ontap;

  Option({Key key, this.text, this.id , this.ontap}) : super(key: key);

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  _OptionState createState() => _OptionState();
}

class _OptionState extends State<Option> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(18),
        color: selected ? secondColor : Colors.transparent,
      ),
      margin: EdgeInsets.all(3),
      child: ListTile(
        title: Text(widget.text),
        trailing: selected
            ? Icon(
                Icons.check,
                color: mainColor,
                size: 24,
              )
            : null,
        onTap: widget.ontap,
      ),
    );
  }
}
