import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/Bles/Model/Responses/home/SearchByNameResponse.dart';
import 'package:qimma_client/pages/home/filter_option.dart';
import 'package:qimma_client/pages/home/single_product_details_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_button2.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';
import 'package:qimma_client/widgets/my_loader.dart';
import 'package:qimma_client/widgets/my_sliver_grid_delegate.dart';
import 'package:qimma_client/widgets/my_text_form_field.dart';

import 'farz_option.dart';

class SearchPage extends StatefulWidget {

  final String search;

  SearchPage({Key key, this.search}) : super(key: key);

  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  void initState() {
    super.initState();

    init();
  }

  init() async {
    homeBloc.homeFilterRequest.name = widget.search;
    if(homeBloc.homeFilterRequest.catId == null) {
      homeBloc.homeFilterRequest.catId = List<int>();
    }
    if(homeBloc.s_get_main_cat.value.data == null){
      await homeBloc.get_main_cat();
    }
    homeBloc.s_get_main_cat.value.data.forEach((element) {
      homeBloc.homeFilterRequest.catId.add(element.id);
    });
    homeBloc.mobHomeSortedFilter(homeBloc.homeFilterRequest);
  }
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(14)),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: AppUtils.translate(context, 'search_result'),
              ),
              SizedBox(
                height: screenUtil.setHeight(10),
              ),
             StreamBuilder<SearchByNameResponse>(
               stream: homeBloc.s_search_by_name.stream,
               builder: (context, snapshot) {
                 if(homeBloc.s_search_by_name.value.loading) {
                   return Column(
                     children: [
                       SizedBox(
                         height: size.width / 2,
                       ),
                       Loader(),
                       SizedBox(
                         height: size.width / 2,
                       ),
                     ],
                   );
                 } else {
                   return Column(
                     children: [
                       Row(
                         mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                         children: [
                           Material(
                             borderRadius: BorderRadius.circular(16),
                             elevation: 4,
                             child: MyButton2(
                               Row(
                                 mainAxisAlignment: MainAxisAlignment.center,
                                 children: [
                                   Text(
                                     AppUtils.translate(context, 'sort'),
                                     style: TextStyle(color: Colors.black, fontSize: 15),
                                   ),
                                   SizedBox(
                                     width: screenUtil.setWidth(8),
                                   ),
                                   Image.asset(
                                     'assets/images/filter.png',
                                     scale: 1.1,
                                   ),
                                 ],
                               ),
                               btnColor: Colors.white,
                               onTap: () {
                                 showSortModelBottomSheet(context);
                               },
                               width: MediaQuery.of(context).size.width / 3,
                             ),

                           ),

                           // Material(
                           //   borderRadius: BorderRadius.circular(16),
                           //   elevation: 4,
                           //   child: MyButton2(
                           //     Row(
                           //       mainAxisAlignment: MainAxisAlignment.center,
                           //       children: [
                           //         Text(
                           //           AppUtils.translate(context, 'filter'),
                           //           style: TextStyle(color: Colors.black, fontSize: 15),
                           //         ),
                           //         SizedBox(
                           //           width: screenUtil.setWidth(8),
                           //         ),
                           //         Icon(
                           //           Icons.sort,
                           //           color: Colors.black,
                           //         ),
                           //       ],
                           //     ),
                           //     btnColor: Colors.white,
                           //     width: MediaQuery.of(context).size.width / 3,
                           //     onTap: () {
                           //       showFilterModelBottomSheet(context);
                           //     },
                           //   ),
                           // ),

                         ],
                       ),
                       SizedBox(height: 10,),
                       MyTextFormField(
                         onChanged: (txt){
                           homeBloc.homeFilterRequest.name = txt.toString() ;
                         },
                         onSubmit: (txt){
                           homeBloc.mobHomeSortedFilter(homeBloc.homeFilterRequest);
                         },
                       ),
                       SizedBox(height: 20,),
                       GridView.builder(
                         shrinkWrap: true,
                         padding: EdgeInsets.zero,
                         physics: NeverScrollableScrollPhysics(),
                         gridDelegate: MySliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
                           crossAxisCount: 2,
                           crossAxisSpacing: 8,
                           mainAxisSpacing: 8,
                           height: size.width * .9,
                         ),
                         itemBuilder: (context, index) {
                           return GestureDetector(
                             onTap: () {
                               Navigator.of(context).push(MaterialPageRoute(builder: (_) => SingleProductDetailsPage(
                                 title: snapshot.data.data[index].Difference,
                                 productId: snapshot.data.data[index].id,
                               ),),);
                             },
                             child: Container(
                               width: size.width / 2,
                               child: Stack(
                                 children: [
                                   CachedNetworkImage(imageUrl: snapshot.data.data[index].image,
                                     width: double.infinity,
                                     height: size.width * .7,
                                     fit: BoxFit.fill,
                                     errorWidget: (_, __, ___) {
                                       return Image.asset('assets/images/no_image.png');
                                     },
                                     placeholder: (_, __) {
                                       return Loader();
                                     },
                                   ),
                                   Positioned(
                                     left: screenUtil.setWidth(5),
                                     right: screenUtil.setWidth(5),
                                     bottom: screenUtil.setHeight(5),
                                     child: Material(
                                       borderRadius: BorderRadius.circular(10),
                                       color: Colors.white,
                                       elevation: 4,
                                       child: Padding(
                                         padding: EdgeInsets.all(screenUtil.setWidth(4.0)),
                                         child: Column(
                                           crossAxisAlignment: CrossAxisAlignment.start,
                                           children: [
                                             Row(
                                               mainAxisAlignment:
                                               MainAxisAlignment.spaceBetween,
                                               children: [
                                                 Flexible(
                                                   child: Text(
                                                     snapshot.data.data[index].Difference,
                                                     style: TextStyle(
                                                       fontSize: screenUtil.setSp(12),
                                                       fontWeight: FontWeight.bold,
                                                     ),
                                                   ),
                                                 ),
                                                 Text(
                                                   '${snapshot.data.data[index].SellingPrice} ${AppUtils.translate(context, "eg")}',
                                                   style: TextStyle(
                                                     fontSize: 14,
                                                     fontWeight: FontWeight.bold,
                                                   ),
                                                 ),
                                               ],
                                             ),
                                             Text(
                                               snapshot.data.data[index].desc,
                                               style: TextStyle(
                                                 color: Colors.grey, fontSize: screenUtil.setSp(12),
                                               ),
                                             ),
                                             Divider(),
                                             // Row(
                                             //   mainAxisAlignment:
                                             //   MainAxisAlignment.spaceEvenly,
                                             //   children: [
                                             //     MyCircleButton(
                                             //       child: Icon(Icons.add_shopping_cart,
                                             //         color: Colors.grey,
                                             //         size: 18,
                                             //       ),
                                             //       onTap: (){
                                             //
                                             //       },
                                             //     ),
                                             //     MyCircleButton(
                                             //       child: Icon(
                                             //         Icons.favorite,
                                             //         size: 18,
                                             //         color: snapshot.data.data[index].isFavorite ? Colors.red : Colors.grey,
                                             //       ),
                                             //       onTap: (){
                                             //
                                             //       },
                                             //     ),
                                             //   ],
                                             // ),
                                           ],
                                         ),
                                       ),
                                     ),
                                   ),
                                 ],
                               ),
                             ),
                           );
                         },
                         itemCount: snapshot.data.data.length,
                       ),
                     ],
                   );
                 }
               },
             ),
            ],
          ),
        ),
      ),
    );
  }

  void showFilterModelBottomSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30),
          topLeft: Radius.circular(30),
        ),
      ),
      builder: (context) => FilterOption(),
    );
  }

  void showSortModelBottomSheet(BuildContext context) {
    showModalBottomSheet(
      context: context,
      isScrollControlled: true,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(30),
          topLeft: Radius.circular(30),
        ),
      ),
      builder: (context) => SortOption(),
    );
  }
}
