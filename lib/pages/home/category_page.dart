import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qimma_client/Bles/Bloc/ProductsBloc.dart';
import 'package:qimma_client/Bles/Model/Responses/products/ProductByCat.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';
import 'package:qimma_client/widgets/my_loader.dart';
import 'package:qimma_client/widgets/my_sliver_grid_delegate.dart';

import 'single_product_details_page.dart';

class CategoryPage extends StatefulWidget {
  final int categoryId;
  final String title;

  CategoryPage({Key key, @required this.title, @required this.categoryId}) : assert(categoryId != null), assert(title != null), super(key: key);

  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {

  final ScreenUtil screenUtil = ScreenUtil();
  @override
  void initState() {
    super.initState();

    productBloc.get_products_by_cat(widget.categoryId);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(14.0),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: widget.title,
              ),
              StreamBuilder<ProductByCatResponse>(
                stream: productBloc.productsByCat.stream,
                builder: (context, snapshot) {
                  if(productBloc.productsByCat.value.loading) {
                    return Column(
                      children: [
                        SizedBox(height: size.width * .5,),
                        Loader(),
                        SizedBox(height: size.width * .5,),
                      ],
                    );
                  } else {
                    return GridView.builder(
                      shrinkWrap: true,
                      padding: EdgeInsets.all(0),
                      physics: NeverScrollableScrollPhysics(),
                      gridDelegate:
                      MySliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
                        crossAxisCount: 2,
                        height: size.width * .75,
                      ),
                      itemBuilder: (context, index) {
                        return CategoryItem(
                          data: snapshot.data.data[index],
                        );
                      },
                      itemCount: snapshot.data.data.length,
                    ); 
                  }
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}


class CategoryItem extends StatefulWidget {

  final CategoryData data;

  const CategoryItem({Key key, this.data}) : super(key: key);

  @override
  _CategoryItemState createState() => _CategoryItemState();
}

class _CategoryItemState extends State<CategoryItem> {

  ScreenUtil screenUtil = ScreenUtil();

  bool favourite = false;

  @override
  void initState() {
    super.initState();

    favourite = widget.data.isFavorite;
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return GestureDetector(
      onTap: () {
        Navigator.of(context).push(MaterialPageRoute(builder: (_) => SingleProductDetailsPage(
          title: widget.data.Difference,
          productId: widget.data.id,),),
        );
      },
      child: Padding(
        padding: EdgeInsets.all(screenUtil.setWidth(8)),
        child: Container(
          child: Stack(
            children: [
              CachedNetworkImage(imageUrl: widget.data.image,
                width: double.infinity,
                height: size.width * .7,
                fit: BoxFit.fill,
                errorWidget: (_, __, ___) {
                  return Image.asset('assets/images/no_image.png');
                },
                placeholder: (_, __) {
                  return Loader();
                },
              ),
              Positioned(
                left: 3,
                right: 3,
                bottom: 5,
                child: Material(
                  borderRadius: BorderRadius.circular(10),
                  color: Colors.white,
                  elevation: 4,
                  child: Padding(
                    padding: EdgeInsets.all(4.0),
                    child: Column(
                      crossAxisAlignment:
                      CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          children: [
                            Flexible(
                              child: Text(
                                widget.data.Difference,
                                style: TextStyle(
                                  fontSize: screenUtil.setSp(12),
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                            Text(
                              "${widget.data.SellingPrice.toString()} ${AppUtils.translate(context, "eg")}",
                              style: TextStyle(
                                fontSize: screenUtil.setSp(12),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ],
                        ),
                        Text(
                          widget.data.desc,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                            color: Colors.grey,
                            fontSize: screenUtil.setSp(10),
                          ),
                        ),
                        Divider(),
                        Row(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceEvenly,
                          children: [
                            MyCircleButton(
                              onTap: () {
                                setState(() {
                                  favourite = !favourite;
                                });

                                if(favourite) {
                                  productBloc.add_wish_list(widget.data.id.toString());
                                }
                              },
                              child: Icon(
                                Icons.favorite,
                                size: 18,
                                color: favourite ? Colors.red : Colors.grey,
                              ),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

