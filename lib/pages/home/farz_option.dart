import 'dart:developer';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/Bles/Model/Requests/HomeFilterRequest.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';

import 'filter_option.dart';

class SortOption extends StatefulWidget {
  @override
  _SortOptionState createState() => _SortOptionState();
}

class _SortOptionState extends State<SortOption> {
  List<String> options;

  final ScreenUtil screenUtil = ScreenUtil();
  int type = 0 ;
  @override
  Widget build(BuildContext context) {
    options = [
      //AppUtils.translate(context, 'best_of_good'),
      AppUtils.translate(context, 'highest_price_to_lowest'),
      AppUtils.translate(context, 'cheaper_price_is_higher'),
      //AppUtils.translate(context, 'popularity'),
      AppUtils.translate(context, 'highest_ratings'),
    ];
    return Padding(
      padding: EdgeInsets.all(screenUtil.setWidth(18)),
      child: Wrap(
        children: [
          MyAppBar(
            text: AppUtils.translate(context, 'sort'),
            actions: [
              MyCircleButton(
                child: Icon(
                  Icons.check,
                  color: Colors.white,
                ),
                color: mainColor,
                onTap: () {
                  // log("tap -----> ");
                  // homeBloc.mobHomeSortedFilter(homeBloc.homeFilterRequest);
                  // Navigator.pop(context);
                },
              ),
            ],
          ),
          ListView.builder(
            shrinkWrap: true,
            physics: NeverScrollableScrollPhysics(),
            itemBuilder: (context, index) {
              return GestureDetector(
                child: Option(
                  text: options[index],
                  ontap: (){
                    log("index -----> ${index}");
                    homeBloc.homeFilterRequest.type = index + 1 ;
                    homeBloc.mobHomeSortedFilter(homeBloc.homeFilterRequest);

                    Navigator.pop(context);
                    setState(() {

                    });
                  },
                ),
                onTap: (){

                  log("index -----> ${index}");
                  homeBloc.homeFilterRequest.type = index ;
                  homeBloc.mobHomeSortedFilter(homeBloc.homeFilterRequest);

                  Navigator.pop(context);
                  setState(() {

                  });
                },
              );
            },
            itemCount: options.length,
          ),
        ],
      ),
    );
  }
}
