
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/CartBloc.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/Bles/Bloc/ProductsBloc.dart';
import 'package:qimma_client/Bles/Model/Requests/AddToCartRequest.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/clickable_text.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';
import 'package:qimma_client/widgets/my_loader.dart';

import 'category_page.dart';
import 'single_product_details_page.dart';

class RecommendedPart extends StatelessWidget {

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return homeBloc.s_home.value.data.highRated == null || homeBloc.s_home.value.data.highRated.isEmpty ? SizedBox.shrink() : Container(
      height: size.width,
      width: size.width,
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(screenUtil.setWidth(8.0)),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  AppUtils.translate(context, 'recommended'),
                  style: TextStyle(fontSize: screenUtil.setSp(16)),
                ),
                ClickableText(
                  text: AppUtils.translate(context, 'show_all'),
                  textStyle: TextStyle(fontSize: screenUtil.setSp(16), color: mainColor),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (_) => CategoryPage(
                          title: AppUtils.translate(context, 'recommended'),
                          categoryId: int.parse(homeBloc.s_home.value.data.highRated[0].mainProductId),
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
          Container(
            width: size.width,
            height: size.width - screenUtil.setHeight(100),
            child: ListView.builder(
              scrollDirection: Axis.horizontal,
              itemBuilder: (context, index) {
                return GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(builder: (_) => SingleProductDetailsPage(
                      title: homeBloc.s_home.value.data.highRated[index].Difference,
                      productId: homeBloc.s_home.value.data.highRated[index].id,),),);
                  },
                  child: Padding(
                    padding: EdgeInsets.all(screenUtil.setWidth(8)),
                    child: Container(
                      width: size.width / 2,
                      child: Stack(
                        children: [
                          CachedNetworkImage(imageUrl: homeBloc.s_home.value.data.highRated[index].image,
                            width: double.infinity,
                            height: size.width * .7,
                            fit: BoxFit.fill,
                            errorWidget: (_, __, ___) {
                              return Image.asset('assets/images/no_image.png');
                            },
                            placeholder: (_, __) {
                              return Loader();
                            },
                          ),
                          Positioned(
                            left: 5,
                            right: 5,
                            bottom: 5,
                            child: Material(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                              elevation: 4,
                              child: Padding(
                                padding: EdgeInsets.all(4.0),
                                child: Column(
                                  crossAxisAlignment:
                                  CrossAxisAlignment.start,
                                  children: [
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      children: [
                                        Flexible(
                                          child: Text(
                                            homeBloc.s_home.value.data.highRated[index].Difference,
                                            style: TextStyle(
                                              fontSize: screenUtil.setSp(12),
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        ),
                                        Text(
                                          "${homeBloc.s_home.value.data.highRated[index].SellingPrice.toString()} ${AppUtils.translate(context, "eg")}",
                                          style: TextStyle(
                                            fontSize: screenUtil.setSp(12),
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ],
                                    ),
                                    Text(
                                      homeBloc.s_home.value.data.highRated[index].desc,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                        color: Colors.grey,
                                        fontSize: screenUtil.setSp(10),
                                      ),
                                    ),
                                    Divider(),
                                    Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceEvenly,
                                      children: [
                                        MyCircleButton(
                                          onTap: (){
                                            Navigator.of(context).push(MaterialPageRoute(builder: (_) => SingleProductDetailsPage(
                                              title: homeBloc.s_home.value.data.highRated[index].Difference,
                                              productId: homeBloc.s_home.value.data.highRated[index].id,),),);
                                          },
                                          child: Image.asset(
                                            'assets/images/cart_img.png',
                                          ),
                                        ),
                                        MyCircleButton(
                                          onTap: (){
                                            productBloc.add_wish_list(homeBloc.s_home.value.data.highRated[index].id.toString());
                                          },
                                          child: Icon(
                                            Icons.favorite,
                                            size: 18,
                                            color: Colors.grey,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                );
              },
              itemCount: homeBloc.s_home.value.data.highRated.length,
            ),
          ),
        ],
      ),
    );
  }
}