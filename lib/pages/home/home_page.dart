import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qimma_client/providers/page_provider.dart';
import 'package:qimma_client/widgets/bottom_navigation_bar.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      extendBody: true,
      bottomNavigationBar: MyBottomNavigatonBar(),
      body: Consumer<PageProvider>(
        builder: (context, provider, child) {
          return provider.page;
        },
      ),
    );
  }
}
