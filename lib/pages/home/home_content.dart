import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/Bles/Bloc/ProfileBloc.dart';
import 'package:qimma_client/Bles/Model/Responses/home/GetMainCat.dart';
import 'package:qimma_client/Bles/Model/Responses/home/HomeResponse.dart';
import 'package:qimma_client/pages/account/account_page.dart';
import 'package:qimma_client/pages/home/search_page.dart';
import 'package:qimma_client/pages/home/shimmer_item.dart';
import 'package:qimma_client/pages/home/slider_part.dart';
import 'package:qimma_client/pages/profile/profile_page.dart';
import 'package:qimma_client/providers/page_provider.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_text_form_field.dart';

import 'ads_part.dart';
import 'most_selling.dart';
import 'recommended_part.dart';
import 'top_categories_part.dart';
import 'offers_part.dart';

class HomeContent extends StatefulWidget {

  @override
  _HomeContentState createState() => _HomeContentState();
}

class _HomeContentState extends State<HomeContent> {
  final ScreenUtil screenUtil = ScreenUtil();

  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {

    homeBloc.get_main_cat(context);
    homeBloc.getHome(context);
    profileBloc.my_info(context);

    Size size = MediaQuery.of(context).size;
    return SingleChildScrollView(
      child: Column(
        children: [
          Container(
            width: double.infinity,
            height: size.width * .6,
            decoration: BoxDecoration(
              color: mainColor,
              borderRadius: BorderRadius.only(
                bottomRight: Radius.circular(45),
                bottomLeft: Radius.circular(45),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.all(screenUtil.setWidth(12.0)),
              child: Column(
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).padding.top,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                        onTap: () {
                          Provider.of<PageProvider>(context, listen: false)
                              .setPage(3, AccountPage());
                        },
                        child: Icon(
                          Icons.menu,
                          color: Colors.white,
                        ),
                      ),
                      Text(
                        AppUtils.translate(context, 'main_page'),
                        style: TextStyle(color: Colors.white, fontSize: screenUtil.setSp(18)),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (_) => ProfilePage(),
                            ),
                          );
                        },
                        child: CircleAvatar(
                          backgroundImage: AppUtils.userData.image == null ?
                          Image.asset('assets/images/avatar.jpg').image :
                          CachedNetworkImageProvider(AppUtils.userData.image),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).padding.top,
                  ),
                  MyTextFormField(
                    bgColor: Colors.white,
                    onSubmit: (String input) {
                      if(input.isEmpty) return;

                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (_) => SearchPage(
                            search: input,
                          ),
                        ),
                      );
                    },
                    prefixIcon: Image.asset('assets/images/search.png'),
                    hintText: AppUtils.translate(context, 'search'),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(
            height: MediaQuery.of(context).padding.top,
          ),
          StreamBuilder<HomeResponse>(
              stream: homeBloc.s_home.stream,
              builder: (context, snapshot) {
                if(homeBloc.s_home.value.loading) {
                  return Column(
                    children: [
                      ShimmerItem(),
                      SizedBox(
                        height: MediaQuery.of(context).padding.top,
                      ),
                    ],
                  );
                } else {
                  return Column(
                    children: [
                      SliderPart(),
                    ],
                  );
                }
              }
          ),
          StreamBuilder<GetMainCatResponse>(
           stream: homeBloc.s_get_main_cat.stream,
           builder: (context, snapshot) {
             if(homeBloc.s_get_main_cat.value.loading) {
               return Column(
                 children: [
                   SizedBox(
                     height: MediaQuery.of(context).padding.top,
                   ),
                   ShimmerItem(
                     height: size.width * .32,
                     width: size.width * .32,
                   ),
                   SizedBox(
                     height: MediaQuery.of(context).padding.top,
                   ),
                 ],
               );
             }
             else {
               return Column(
                 children: [
                   SizedBox(
                     height: MediaQuery.of(context).padding.top,
                   ),
                   CategoriesPart(),
                   SizedBox(
                     height: MediaQuery.of(context).padding.top,
                   ),
                 ],
               );
             }
           }
         ),
          StreamBuilder<HomeResponse>(
            stream: homeBloc.s_home.stream,
            builder: (context, snapshot) {
              if(homeBloc.s_home.value.loading) {
                return Column(
                  children: [
                    ShimmerItem(),
                    SizedBox(
                      height: MediaQuery.of(context).padding.top,
                    ),
                    ShimmerItem(),
                    SizedBox(
                      height: MediaQuery.of(context).padding.top,
                    ),
                    ShimmerItem(),
                    SizedBox(
                      height: MediaQuery.of(context).padding.top,
                    ),
                    ShimmerItem(),
                    SizedBox(
                      height: MediaQuery.of(context).padding.top * 3,
                    ),
                  ],
                );
              } else {
                return Column(
                  children: [
                    // AdsPart(),
                    // SizedBox(
                    //   height: MediaQuery.of(context).padding.top,
                    // ),
                    RecommendedPart(),
                    SizedBox(
                      height: MediaQuery.of(context).padding.top,
                    ),
                    OffersPart(),
                    SizedBox(
                      height: MediaQuery.of(context).padding.top,
                    ),
                    MostSellingPart(),
                    SizedBox(
                      height: MediaQuery.of(context).padding.top * 3,
                    ),
                  ],
                );
              }
            }
          ),
        ],
      ),
    );
  }

}

class TopPartCategory {
  String name;
  String image;

  TopPartCategory(this.name, this.image);
}
