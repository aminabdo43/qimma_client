import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/pages/home/category_page.dart';
import 'package:qimma_client/widgets/my_loader.dart';

class CategoriesPart extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Container(
      height: size.width * .32,
      width: size.width,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (_) => CategoryPage(title: homeBloc.s_get_main_cat.value.data[index].name, categoryId: homeBloc.s_get_main_cat.value.data[index].id)));
            },
            child: Container(
              margin: EdgeInsets.symmetric(horizontal: 10),
              width: size.width * .32,
              height: size.width * .32,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(15),
              ),
              child: Stack(
                children: [
                  ClipRRect(
                    borderRadius: BorderRadius.circular(15),
                    child: CachedNetworkImage(
                      imageUrl: homeBloc.s_get_main_cat.value.data[index].image,
                      height: size.width * .32,
                      width: size.width,
                      fit: BoxFit.fill,
                      errorWidget: (_, __, ___) {
                        return Image.asset('assets/images/no_image.png',
                          height: size.width * .32,
                          width: size.width,
                          fit: BoxFit.fill,
                        );
                      },
                      placeholder: (_, __) {
                        return Loader(size: 50,);
                      },
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10),
                      width: size.width * .32,
                      height: size.width * .08,
                      decoration: BoxDecoration(
                        color: Colors.black26,
                        borderRadius: BorderRadius.only(
                          bottomLeft: Radius.circular(15),
                          bottomRight: Radius.circular(15),
                        ),
                      ),
                      child: Center(
                        child: Text('${homeBloc.s_get_main_cat.value.data[index].name}', style: TextStyle(color: Colors.white, fontSize: 18, fontWeight: FontWeight.w500),),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          );
        },
        itemCount: homeBloc.s_get_main_cat.value.data.length,
      ),
    );
  }
}
