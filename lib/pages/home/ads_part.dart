
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/pages/home/single_product_details_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/widgets/my_button.dart';
import 'package:qimma_client/widgets/my_loader.dart';

class AdsPart extends StatelessWidget {

  final ScreenUtil screenUtil = ScreenUtil();

  final List<Color> colors = [
    Color(0xffF8B8BA),
    Color(0xff7ED4D0),
  ];

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return homeBloc.s_home.value.data.Offers == null || homeBloc.s_home.value.data.Offers.isEmpty ? SizedBox.shrink() : Container(
      width: size.width,
      height: size.width * .6,
      child: ListView.builder(
        itemBuilder: (context, index) {
          return Container(
            margin: EdgeInsets.symmetric(horizontal: screenUtil.setWidth(8)),
            width: size.width * .85,
            height: size.width * .6,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(18),
              color: colors[index % 2 == 0 ? 0 : 1],
            ),
            child: Row(
              children: [
                Expanded(
                  flex: 4,
                  child: Padding(
                    padding: EdgeInsets.all(12.0),
                    child: CachedNetworkImage(
                      imageUrl: homeBloc.s_home.value.data.Offers[index].image,
                      placeholder: (context, url) {
                        return Loader(size: 50,);
                      },
                      errorWidget: (_, __, ___) {
                        return Image.asset('assets/images/no_image.png',);
                      },
                    ),
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Padding(
                    padding: EdgeInsets.symmetric(vertical: screenUtil.setHeight(10), horizontal: screenUtil.setWidth(8)),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(
                              homeBloc.s_home.value.data.Offers[index].Difference,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: screenUtil.setSp(20),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            Row(
                              children: [
                                Text(
                                  homeBloc.s_home.value.data.Offers[index].offerAmount,
                                  style:
                                  TextStyle(color: Colors.white, fontSize: screenUtil.setSp(16)),
                                ),
                                Text(
                                  ' Off',
                                  style:
                                  TextStyle(color: Colors.white, fontSize: screenUtil.setSp(16)),
                                ),
                              ],
                            ),
                            Text(
                              '${homeBloc.s_home.value.data.Offers[index].SellingPrice} ${AppUtils.translate(context, "eg")}',
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: screenUtil.setSp(16),
                              ),
                            ),
                          ],
                        ),
                        MyButton(
                          AppUtils.translate(context, 'shop_now'),
                          height: screenUtil.setHeight(50),
                          width: size.width * .3,
                          onTap: () {
                            Navigator.of(context).push(MaterialPageRoute(builder: (_) => SingleProductDetailsPage(
                              title: homeBloc.s_home.value.data.Offers[index].Difference,
                              productId: homeBloc.s_home.value.data.Offers[index].id,
                            ),),);
                          },
                          textStyle: TextStyle(color: Colors.white, fontSize: screenUtil.setSp(13)),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(3),
                            border: Border.all(
                              color: Colors.white,
                              width: 1,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          );
        },
        itemCount: homeBloc.s_home.value.data.Offers.length,
        scrollDirection: Axis.horizontal,
      ),
    );
  }
}