
import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/HomeBloc.dart';
import 'package:qimma_client/pages/home/single_product_details_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/clickable_text.dart';
import 'package:qimma_client/widgets/my_loader.dart';

import 'category_page.dart';

class SliderPart extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return homeBloc.s_home.value.data.sliders == null || homeBloc.s_home.value.data.sliders.isEmpty ? SizedBox.shrink() : Container(
      height: size.width * .8,
      width: size.width,
      child: Column(
        children: [
          Expanded(
            child: Container(
              width: size.width,
              child: CarouselSlider(
                options: CarouselOptions(
                  height: 200,
                  aspectRatio: 16/9,
                  viewportFraction: 0.8,
                  initialPage: 0,
                  enableInfiniteScroll: true,
                  reverse: false,
                  autoPlay: true,
                  autoPlayInterval: Duration(seconds: 3),
                  autoPlayAnimationDuration: Duration(milliseconds: 800),
                  autoPlayCurve: Curves.fastOutSlowIn,
                  enlargeCenterPage: true,
                  onPageChanged: null,
                  scrollDirection: Axis.horizontal,
                ),
                items:  homeBloc.s_home.value.data.sliders.map((i) {
                  return Builder(
                    builder: (BuildContext context) {
                      return Container(
                          width: MediaQuery.of(context).size.width,
                          margin: EdgeInsets.symmetric(horizontal: 5.0),
                          decoration: BoxDecoration(
                              color: Colors.amber
                          ),
                          child: CachedNetworkImage(
                            imageUrl: i.image,
                            width: double.infinity,
                            height: size.width * .7,
                            fit: BoxFit.fill,
                            errorWidget: (_, __, ___) {
                              return Image.asset('assets/images/no_image.png', width: double.infinity,
                                height: size.width * .7,
                                fit: BoxFit.fill,
                              );
                            },
                            placeholder: (_, __) {
                              return Loader();
                            },
                          ),

                      );
                    },
                  );
                }).toList(),
              )
            ),
          ),
        ],
      ),
    );
  }
}