import 'dart:developer';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/OrderBloc.dart';
import 'package:qimma_client/Bles/Bloc/ProfileBloc.dart';
import 'package:qimma_client/Bles/Model/Requests/AddOrderRequest.dart';
import 'package:qimma_client/Bles/Model/Responses/profile/UserAddressResponse.dart';
import 'package:qimma_client/pages/address/add_new_address_page.dart';
import 'package:qimma_client/pages/address/delivery_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/clickable_text.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_button.dart';
import 'package:qimma_client/widgets/my_loader.dart';
import 'package:qimma_client/widgets/my_text_form_field.dart';

class PaymentPage extends StatefulWidget {
  @override
  _PaymentPageState createState() => _PaymentPageState();
}

class _PaymentPageState extends State<PaymentPage> {
  int selectedAddress = 0;
  int selectedPayment = 0;

  bool inmacca = true;
  File bankTransferImage;

  TextEditingController bankNameController = TextEditingController();
  TextEditingController noteController = TextEditingController();

  final ScreenUtil screenUtil = ScreenUtil();

  bool gotData = false;

  void onAddressSelected(int val) {
    log("address selected num -> ${val}");
    selectedAddress = val;
    setState(() {});
  }

  void onPaymentSelected(int val) {
    selectedPayment = val;
    setState(() {});
  }

  @override
  void initState() {
    super.initState();

    profileBloc.user_address();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;

    return Scaffold(
      backgroundColor: Colors.white,
      body: Column(
        children: [
          Container(
            color: Colors.white,
            child: Padding(
              padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  SizedBox(
                    height: MediaQuery.of(context).padding.top,
                  ),
                  MyAppBar(
                    text: AppUtils.translate(context, 'shipping'),
                  ),
                ],
              ),
            ),
          ),
          StreamBuilder<UserAddressResponse>(
            stream: profileBloc.s_user_address.stream,
            builder: (context, snapshot) {
              if(profileBloc.s_user_address.value.loading) {
                return Column(
                  children: [
                    SizedBox(height: size.width,),
                    Loader(),
                  ],
                );
              } else {
                // if(!gotData) {
                //   selectedAddress = snapshot.data?.data != null && snapshot.data?.data.isNotEmpty ? snapshot.data?.data[0].id : 0;
                // }
                return Expanded(
                  child: Container(
                    width: size.width,
                    decoration: BoxDecoration(
                      color: scaffoldBackgroundColor,
                      borderRadius: BorderRadius.only(
                        topRight: Radius.circular(28),
                        topLeft: Radius.circular(28),
                      ),
                    ),
                    child: SingleChildScrollView(
                      child: Padding(
                        padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              children: List.generate(
                                snapshot.data.data.length, (index) => Padding(
                                  padding: EdgeInsets.all(screenUtil.setWidth(8.0)),
                                  child: Material(
                                    elevation: 5,
                                    borderRadius: BorderRadius.circular(20),
                                    child: RadioListTile(
                                      value: snapshot.data.data[index].id,
                                      groupValue: selectedAddress,
                                      onChanged: (int i){
                                        setState(() {
                                          log(i.toString());
                                          selectedAddress = i;
                                        });
                                      },
                                      // onChanged: onAddressSelected,
                                      title: Text(snapshot.data.data[index].name),
                                      subtitle: Text(
                                        snapshot.data.data[index].address,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            // add new address
                            Center(
                              child: ClickableText(
                                text: AppUtils.translate(context, 'add_new_address'),
                                onTap: () {
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (_) => AddNewAddressPage(),
                                    ),
                                  );
                                },
                                textStyle: TextStyle(
                                  fontSize: screenUtil.setSp(20),
                                  color: mainColor,
                                  decoration: TextDecoration.underline,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: MediaQuery.of(context).padding.top - screenUtil.setHeight(15.0),
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: [

                                MaterialButton(
                                  color: mainColor,
                                  textColor: Colors.white,
                                  onPressed: (){
                                  inmacca = true ;
                                  setState(() {});
                                } ,
                                  child: Text("${AppUtils.translate(context, "inmacca")}"),
                                ),
                                MaterialButton(
                                  color: mainColor,
                                  textColor: Colors.white,
                                  onPressed: (){
                                  inmacca = false ;
                                  setState(() {});
                                  } ,
                                  child: Text(
                                    "${AppUtils.translate(context, "outmacca")}"
                                  ),
                                ),
                              ],
                            ),

                            if(!inmacca)
                            Column(
                              children: [
                                Text(AppUtils.translate(context, 'select_payment_method'), style: TextStyle(fontSize: 18),),
                                SizedBox(
                                  height: MediaQuery.of(context).padding.top,
                                ),
                                Text(AppUtils.translate(context, 'cash'), style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold),),
                                SizedBox(
                                  height: MediaQuery.of(context).padding.top,
                                ),

                                Text("${AppUtils.translate(context, 'bank_name')}"),
                                MyTextFormField(
                                  maxLines: 1,
                                  controller: bankNameController,
                                  keyboardType: TextInputType.text,
                                  hintText: AppUtils.translate(context, 'bank_name'),
                                ),
                                SizedBox(
                                  height: 10,
                                ),
                                Text("${AppUtils.translate(context, 'note')}"),
                                MyTextFormField(
                                  maxLines: 5,
                                  controller: noteController,
                                  keyboardType: TextInputType.text,
                                  hintText: AppUtils.translate(context, 'note'),
                                ),
                                SizedBox(
                                  height: MediaQuery.of(context).padding.top,
                                ),
                                // bank image
                                Center(child: Text("صورة التحويل")),
                                Center(
                                  child: GestureDetector(
                                    onTap: () async {
                                      var permissionGranted = await AppUtils.askPhotosPermission();
                                      if(permissionGranted) {
                                        var image = await AppUtils.getImage(1);
                                        if(image != null) {
                                          setState(() {
                                            bankTransferImage = image[0];
                                          });
                                        }
                                      } else {
                                        AppUtils.showToast(msg: AppUtils.translate(context, 'permission_denied'));
                                      }
                                    },
                                    child: CircleAvatar(
                                      radius: 50,
                                      backgroundImage: bankTransferImage == null
                                          ? Image.asset('assets/images/no_image.png').image
                                          : Image.file(bankTransferImage).image,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            MyButton(AppUtils.translate(context, 'complete_order'), onTap: () async {

                              profileBloc.s_user_address.value.loading = true ;
                              profileBloc.s_user_address.value = profileBloc.s_user_address.value;
                              await orderBloc.addOrder(AddOrderRequest(paymentMethod: "1", addressId: "$selectedAddress"
                              , bank_name: bankNameController.text, notes: noteController.text,bank_trans_receipt: bankTransferImage
                              ));
                              profileBloc.s_user_address.value.loading = false ;
                              profileBloc.s_user_address.value = profileBloc.s_user_address.value;

                              Navigator.of(context).push(MaterialPageRoute(builder: (_) => DeliverPage(),),);
                            }),
                          ],
                        ),
                      ),
                    ),
                  ),
                );
              }
            }
          ),
        ],
      ),
    );
  }
}
