import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:qimma_client/Bles/Bloc/CartBloc.dart';
import 'package:qimma_client/Bles/Model/Requests/AddToCartRequest.dart';
import 'package:qimma_client/Bles/Model/Requests/UpdateCartRequest.dart';
import 'package:qimma_client/Bles/Model/Responses/cart/MyCartResponse.dart';
import 'package:qimma_client/pages/cart/cart_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_loader.dart';

class CartItem extends StatefulWidget {
  final bool delete;
  final ProductsCartBean cartItem;
  final Function onDeleteItem;

  const CartItem({Key key, this.delete = false, @required this.cartItem, this.onDeleteItem}) : assert(cartItem != null), super(key: key);

  @override
  _CartItemState createState() => _CartItemState();
}

class _CartItemState extends State<CartItem> {
  int counter = 1;
  bool deleteCurrentItem = false;

  @override
  void initState() {
    super.initState();

    counter = widget.cartItem?.quantity ?? 1;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 4,
        vertical: 8,
      ),
      child: Row(
        children: [
          widget.delete ? GestureDetector(
            onTap: () {
              setState(() {
                deleteCurrentItem = !deleteCurrentItem;
              });
            },
            child: Container(
              width: 30,
              height: 30,
              decoration: BoxDecoration(
                color: mainColor.withOpacity(.15),
                shape: BoxShape.circle,
              ),
              child: Center(
                child: deleteCurrentItem ? Icon(Icons.close, size: 20, color: Colors.black,) : Text(''),
              ),
            ),
          ) : null,
          widget.delete ? SizedBox(width: 8,) : null,
          Expanded(
            child: Material(
              elevation: 4,
              borderRadius: BorderRadius.circular(18),
              child: Padding(
                padding: EdgeInsets.all(18.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Row(
                      children: [
                        widget.cartItem.image == null ? Image.asset(
                          'assets/images/no_image.png',
                          height: 80,
                          width: 80,
                          fit: BoxFit.fill,
                        ) : CachedNetworkImage(
                          imageUrl: widget.cartItem.image,
                          height: 80,
                          width: 80
                          , errorWidget: (_, __, ___) {
                          return Image.asset(
                            'assets/images/no_image.png',
                            height: 80,
                            width: 80,
                            fit: BoxFit.fill,
                          );
                        },
                          placeholder: (_, __) {
                          return Loader(size: 30,);
                          },
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text(widget.cartItem.productDetailDifference),
                            Text(
                              '${widget.cartItem.price} ${AppUtils.translate(context, "eg")}',
                              style: TextStyle(
                                color: mainColor,
                                fontSize: 16,
                              ),
                            ),
                            SizedBox(height: 10,),
                            GestureDetector(child: Icon(Icons.delete), onTap: () {
                              widget.onDeleteItem();
                            },),
                          ],
                        ),
                      ],
                    ),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              counter++;
                            });
                            
                           updateCart();
                          },
                          child: Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                              color: mainColor,
                              shape: BoxShape.circle,
                            ),
                            child: Center(
                              child: Icon(
                                Icons.add,
                                size: 18,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 3,
                        ),
                        Text('$counter'),
                        SizedBox(
                          height: 3,
                        ),
                        GestureDetector(
                          onTap: () {
                            setState(() {
                              if (counter > 1) {
                                counter--;
                              }

                              updateCart();
                            });
                          },
                          child: Container(
                            width: 30,
                            height: 30,
                            decoration: BoxDecoration(
                              color: mainColor,
                              shape: BoxShape.circle,
                            ),
                            child: Center(
                              child: Icon(
                                Icons.remove,
                                size: 18,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ].where((element) => element != null).toList(),
      ),
    );
  }

  void updateCart() async {

    // List<ProductsBean> products = List<ProductsBean>();
    // cartBloc.my_cart.value.data.products.forEach((element) {
    //   if(widget.cartItem.id == element.id){
    //     products.add(ProductsBean(
    //       colorId: widget.cartItem.color?.id ?? 0,
    //       productDetailId: widget.cartItem?.id ?? 0,
    //       quantity: counter,
    //       sizeId: widget.cartItem.size?.id ?? 0,
    //     ));
    //   }
    //   else{
    //     products.add(ProductsBean(
    //         colorId: element?.color?.id ?? 0,
    //         productDetailId: element.id,
    //         quantity: element.quantity,
    //         sizeId: element?.size?.id ?? 0
    //     ));
    //   }
    // });
    //
    // cartBloc.updateCart(UpdateCartRequest(
    //     products: products
    // ));

    cartBloc.addToCart(AddToCartRequest(
      productId:  widget.cartItem?.id.toString() ,
      colorId: widget.cartItem.color?.id?.toString() ?? null,
      sizeId: widget.cartItem.size?.id?.toString() ?? null,
      quantity: counter.toString()
    ));
    
    AppUtils.showAlertDialog(
      context: context,
      continue_: (){
        Navigator.of(context).pop();
      },
      goToCart: (){
        Navigator.of(context).push(MaterialPageRoute(builder: (_) => CartPage()));
      },
      header: "هل تريد اكمال التسوق",
      content: "هل تريد اكمال التسوق"
    );
  }
}
