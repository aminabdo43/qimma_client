import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:provider/provider.dart';
import 'package:qimma_client/Bles/Bloc/CartBloc.dart';
import 'package:qimma_client/Bles/Model/Responses/cart/MyCartResponse.dart';
import 'package:qimma_client/pages/cart/cart_item.dart';
import 'package:qimma_client/pages/cart/payment_page.dart';
import 'package:qimma_client/pages/home/home_content.dart';
import 'package:qimma_client/providers/page_provider.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_button2.dart';
import 'package:qimma_client/widgets/my_loader.dart';
import 'package:qimma_client/widgets/my_text_form_field.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  bool hasProducts = true;
  bool delete = false;

  Duration duration = Duration(milliseconds: 500);

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  void initState() {
    super.initState();

    cartBloc.getMyCart();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        Provider.of<PageProvider>(context, listen: false)
            .setPage(0, HomeContent());

        return false;
      },
      child: Scaffold(
        backgroundColor: Colors.white,
        body: Column(
          children: [
            Container(
              color: Colors.white,
              child: Padding(
                padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: MediaQuery.of(context).padding.top,
                    ),
                    MyAppBar(
                      text: AppUtils.translate(context, 'cart'),
                      onBackBtnPressed: () {
                        Provider.of<PageProvider>(context, listen: false)
                            .setPage(0, HomeContent());
                      },
                    ),
                    SizedBox(
                      height: hasProducts
                          ? MediaQuery.of(context).padding.top
                          : MediaQuery.of(context).padding.top * 2,
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              child: StreamBuilder<MyCartResponse>(
                stream: cartBloc.my_cart.stream,
                builder: (context, snapshot) {
                  return Column(
                    children: [
                      if (cartBloc.my_cart.value.loading)
                        Expanded(child: Loader())
                      else snapshot.data?.data?.products?.isNotEmpty ?? false
                            ? Expanded(
                                child: Container(
                                  width: size.width,
                                  decoration: BoxDecoration(
                                    color: scaffoldBackgroundColor,
                                    borderRadius: BorderRadius.only(
                                      topRight: Radius.circular(28),
                                      topLeft: Radius.circular(28),
                                    ),
                                  ),
                                  child: Padding(
                                    padding: EdgeInsets.all(
                                        screenUtil.setWidth(14.0),
                                    ),
                                    child: SingleChildScrollView(
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          ListView.builder(
                                            padding: EdgeInsets.zero,
                                            shrinkWrap: true,
                                            physics: NeverScrollableScrollPhysics(),
                                            itemBuilder: (context, index) {
                                              return CartItem(
                                                delete: delete,
                                                cartItem: snapshot.data.data.products[index],
                                                onDeleteItem: () async {
                                                  cartBloc.deleteFromCart(snapshot.data.data.products[index].id);
                                                  snapshot.data.data.products.removeAt(index);
                                                  setState(() {});
                                                },
                                              );
                                            },
                                            itemCount:
                                                snapshot.data.data.products.length,
                                          ),
                                          SizedBox(
                                            height: screenUtil.setHeight(20),
                                          ),
                                          MyTextFormField(
                                            bgColor: Colors.white,
                                            hintText: AppUtils.translate(context, 'coupon'),
                                          ),
                                          SizedBox(
                                            height: screenUtil.setHeight(20),
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(AppUtils.translate(context, '${AppUtils.translate(context, "subtotal")}')),
                                              Text(AppUtils.translate(context, '${snapshot.data.data.subTotalPrice} ${AppUtils.translate(context, "eg")}')),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(AppUtils.translate(context, '${AppUtils.translate(context, "taxes")}')),
                                              Text(AppUtils.translate(context, '${snapshot.data.data.tax}  %')),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(AppUtils.translate(context, '${AppUtils.translate(context, "delivery")}')),
                                              Text(AppUtils.translate(context, '${snapshot.data.data.deliveryCharges}  ${AppUtils.translate(context, "eg")}')),
                                            ],
                                          ),
                                          Row(
                                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(AppUtils.translate(context, '${AppUtils.translate(context, "total")}')),
                                              Text(AppUtils.translate(context, '${snapshot.data.data.totalPrice}  ${AppUtils.translate(context, "eg")}')),
                                            ],
                                          ),

                                          SizedBox(
                                            height: screenUtil.setHeight(20),
                                          ),
                                          Container(
                                            width: size.width,
                                            padding: EdgeInsets.all(screenUtil.setWidth(10)),
                                            decoration: BoxDecoration(
                                              color: Colors.white,
                                              borderRadius:
                                                  BorderRadius.circular(20),
                                            ),
                                            child: Row(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: [
                                                MyButton2(
                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.center,
                                                    children: [
                                                      Image.asset(
                                                        'assets/images/payment.png',
                                                      ),
                                                      SizedBox(
                                                        width: screenUtil.setWidth(8),
                                                      ),
                                                      Text(
                                                        AppUtils.translate(context, 'complete_order'), style: TextStyle(color: Colors.white),
                                                      ),
                                                    ],
                                                  ),
                                                  width: size.width / 3,
                                                  onTap: () {
                                                    Navigator.push(context, MaterialPageRoute(builder: (_) => PaymentPage(),),);
                                                  },
                                                  height: screenUtil.setHeight(60),
                                                ),
                                              ],
                                            ),
                                          ),
                                          SizedBox(
                                            height: screenUtil.setHeight(90),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            : Image.asset('assets/images/no_products.png'),
                    ],
                  );
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
