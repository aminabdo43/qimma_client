
import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/screenutil.dart';
import 'package:qimma_client/Bles/Bloc/ProfileBloc.dart';
import 'package:qimma_client/pages/home/single_product_details_page.dart';
import 'package:qimma_client/utils/app_utils.dart';
import 'package:qimma_client/utils/consts.dart';
import 'package:qimma_client/widgets/my_app_bar.dart';
import 'package:qimma_client/widgets/my_circle_btn.dart';
import 'package:qimma_client/widgets/my_loader.dart';
import 'package:qimma_client/widgets/my_sliver_grid_delegate.dart';
import 'package:qimma_client/Bles/Model/Responses/profile/MyWishListResponse.dart';

class WishlistPage extends StatefulWidget {

  @override
  _WishlistPageState createState() => _WishlistPageState();
}

class _WishlistPageState extends State<WishlistPage> {

  final ScreenUtil screenUtil = ScreenUtil();

  @override
  void initState() {
    super.initState();

    profileBloc.my_wishlist();
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: scaffoldBackgroundColor,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.all(screenUtil.setWidth(14.0)),
          child: Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).padding.top,
              ),
              MyAppBar(
                text: AppUtils.translate(context, 'wishlist'),
              ),
              StreamBuilder<MyWishListResponse>(
                stream: profileBloc.s_my_wishlist.stream,
                builder: (context, snapshot) {
                 if(profileBloc.s_my_wishlist.value.loading) {
                   return Column(
                     children: [
                       SizedBox(height: size.width * .5,),
                       Loader(),
                       SizedBox(height: size.width * .5,),
                     ],
                   );
                 } else {
                   return snapshot.data.data.isEmpty ? Column(
                     children: [
                       SizedBox(height: size.width * .5,),
                       Text('No Products'),
                       SizedBox(height: size.width * .5,),
                     ],
                   ) : GridView.builder(
                     shrinkWrap: true,
                     physics: NeverScrollableScrollPhysics(),
                     gridDelegate:
                     MySliverGridDelegateWithFixedCrossAxisCountAndFixedHeight(
                       crossAxisCount: 2,
                       crossAxisSpacing: 8,
                       mainAxisSpacing: 8,
                       height: size.width * .7,
                     ),
                     itemBuilder: (context, index) {
                       return GestureDetector(
                         onTap: () {
                           Navigator.of(context).push(
                             MaterialPageRoute(
                               builder: (_) => SingleProductDetailsPage(
                                 productId: snapshot.data.data[index].id,
                                 title: snapshot.data.data[index].Difference,
                               ),
                             ),
                           );
                         },
                         child: Container(
                           width: size.width / 2,
                           child: Stack(
                             children: [
                               Container(
                                 width: double.infinity,
                                 height: size.width * .7,
                                 decoration: BoxDecoration(
                                   borderRadius: BorderRadius.circular(18),
                                   image: DecorationImage(
                                     image: CachedNetworkImageProvider(snapshot.data.data[index].image),
                                     fit: BoxFit.fill,
                                   ),
                                 ),
                               ),
                               Positioned(
                                 left: screenUtil.setWidth(5),
                                 right: screenUtil.setWidth(5),
                                 bottom: screenUtil.setHeight(5),
                                 child: Material(
                                   borderRadius: BorderRadius.circular(10),
                                   color: Colors.white,
                                   elevation: 4,
                                   child: Padding(
                                     padding: EdgeInsets.all(screenUtil.setWidth(4)),
                                     child: Column(
                                       crossAxisAlignment:
                                       CrossAxisAlignment.start,
                                       children: [
                                         Row(
                                           mainAxisAlignment:
                                           MainAxisAlignment.spaceBetween,
                                           children: [
                                             Flexible(
                                               child: Text(
                                                 snapshot.data.data[index].Difference,
                                                 style: TextStyle(
                                                   fontSize: screenUtil.setSp(12),
                                                   fontWeight: FontWeight.bold,
                                                 ),
                                               ),
                                             ),
                                             Text(
                                               "${snapshot.data.data[index]?.SellingPrice}  ${AppUtils.translate(context, "eg")}",
                                               style: TextStyle(
                                                 fontSize: screenUtil.setSp(12),
                                                 fontWeight: FontWeight.bold,
                                               ),
                                             ),
                                           ],
                                         ),
                                         Text(
                                           snapshot.data.data[index].desc,
                                           style: TextStyle(
                                             color: Colors.grey,
                                             fontSize: screenUtil.setSp(10),
                                           ),
                                         ),
                                         // Divider(),
                                         // Row(
                                         //   mainAxisAlignment:
                                         //   MainAxisAlignment.spaceEvenly,
                                         //   children: [
                                         //     MyCircleButton(
                                         //       child: Image.asset(
                                         //         'assets/images/cart_img.png',
                                         //       ),
                                         //     ),
                                         //     MyCircleButton(
                                         //       child: Image.asset(
                                         //         'assets/images/toggle_img.png',
                                         //       ),
                                         //     ),
                                         //     MyCircleButton(
                                         //       child: Icon(
                                         //         Icons.favorite,
                                         //         size: screenUtil.setSp(18),
                                         //         color: mainColor,
                                         //       ),
                                         //     ),
                                         //   ],
                                         // ),
                                       ],
                                     ),
                                   ),
                                 ),
                               ),
                             ],
                           ),
                         ),
                       );
                     },
                     itemCount: snapshot.data.data.length,
                   );
                 }
                }
              ),
            ],
          ),
        ),
      ),
    );
  }
}
